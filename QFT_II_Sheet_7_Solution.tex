\documentclass[10pt]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage[a4paper,width=150mm,top=25mm,bottom=25mm]{geometry}
\usepackage{enumerate} %Roman enumeration
\usepackage{dsfont}
\usepackage{sectsty}
\usepackage{mathtools}
\usepackage{float}
\usepackage{physics}
\usepackage{braket}
\usepackage{pdfpages}
\usepackage{mathrsfs}
\usepackage{cancel}
\usepackage{accents}
\usepackage{hyperref}
\renewcommand*{\thepage}{\large\arabic{page}}

\allsectionsfont{\centering}

% New commands
\newcommand*{\QED}{\hfill\ensuremath{\blacksquare}}%
\newcommand*{\QEDB}{\hfill\ensuremath{\square}}%
\renewcommand*{\i}{\mathrm{i}}%
\newcommand*{\allimplies}{\big\} \hspace*{-0.2cm} \implies }%
\newcommand\hteq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny !}}}{=}}}
\newcommand\oneeq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny (i)}}}{=}}}
\newcommand\indeq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny ind}}}{=}}}
\newcommand\stareq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny \ref{eq-Lemma_1}}}}{=}}}


\begin{document}
% Original sheet
\includepdf[pages=-]{sheet7.pdf}
% Exercise 1
\noindent\rule{\textwidth}{2pt}
\textbf{Exercise 1 -- Solution:}\\\
\begin{addmargin}[1cm]{0cm}
%Text here
First we have to introduce 'Gauge Fixing', since otherwise we can not compute our integral over $\mathcal{D} A$. To do this we follow the routine proposed by Faddeev and Popov. We start with the calculation of an expectation value and introduce an $\mathds{1}$:
\begin{alignat*}{2}
\braket{\Gamma[A,\phi]}&=\frac{\int{\mathcal{D}\phi\mathcal{D}A\, \Gamma[A,\phi] e^{\i S}}}{\int{\mathcal{D}\phi\mathcal{D}A \, e^{\i S}}}\\
&=\frac{\int{\mathcal{D}\phi\mathcal{D}A\, \Gamma[A,\phi]\, \Delta_{FP} \Delta_{FP}^{-1} e^{\i S}}}{\int{\mathcal{D}\phi\mathcal{D}A \,\Delta_{FP}\Delta_{FP}^{-1} e^{\i S}}} \: \: &&\bigg|\Delta_{FP}^{-1}=\int{\mathcal{D}g\, \delta(G^a(^{g}A))} \\
&=\frac{\int{\mathcal{D}\phi\mathcal{D}A\, \Gamma[A,\phi]\, \Delta_{FP} \delta(\partial_{\mu} A^{\mu}-\chi) e^{\i S}}}{\int{\mathcal{D}\phi\mathcal{D}A \,\Delta_{FP}\delta(\partial_{\mu} A^{\mu}-\chi) e^{\i S}}} \\
&= \frac{\int{\mathcal{D}\phi\mathcal{D}A \mathcal{D}\chi \, \Gamma[A,\phi]\, \Delta_{FP} \delta(\partial_{\mu} A^{\mu}-\chi) e^{\i S-\i \frac{1}{2\xi}(\chi)^2}}}{\int{\mathcal{D}\phi\mathcal{D}A\mathcal{D}\chi \,\Delta_{FP}\delta(\partial_{\mu} A^{\mu}-\chi) e^{\i S-\i \frac{1}{2\xi}(\chi)^2}}} \\
&= \frac{\int{\mathcal{D}\phi\mathcal{D}A  \, \Gamma[A,\phi]\, \Delta_{FP}  e^{\i S-\i \frac{1}{2\xi}(\partial_{\mu}A^{\mu})^2}}}{\int{\mathcal{D}\phi\mathcal{D}A \,\Delta_{FP} e^{\i S-\i \frac{1}{2\xi}(\partial_{\mu}A^{\mu})^2}}} \quad &&\bigg|\Delta_{FP}=const. \\
&= \frac{\int{\mathcal{D}\phi\mathcal{D}A  \, \Gamma[A,\phi]\, e^{\i S-\i \frac{1}{2\xi}(\partial_{\mu}A^{\mu})^2}}}{\int{\mathcal{D}\phi\mathcal{D}A \, e^{\i S-\i \frac{1}{2\xi}(\partial_{\mu}A^{\mu})^2}}}
\end{alignat*}
We were able to cancel the Faddeev-Popov determinant $\Delta_{FP}$, since it was independent of the fields, i.e. in the abelian theory ghost do not couple to the fields. Now our quadratic form in $A$ is invertible. We get an additional term in our Lagrangian responsible for the 'Gauge Fixing'. \\\\
We start from the Lagrangian density for massless scalar QED:
\begin{equation*}
\mathscr{L}=-\frac{1}{4}F^{\mu\nu}F_{\mu\nu}+(\mathcal{D_{\mu}}\phi)(\mathcal{D^{\mu}}\phi)^\star-\frac{1}{2\xi}(\partial_{\mu}A^{\mu})^2
\end{equation*}
With $\mathcal{D_{\mu}}=\partial_{\mu}+ie\mathcal{A_{\mu}}$. We will write the Lagrangian above in terms of real fields $\varphi_1$ and $\varphi_2$. The whole field is now expressed by $\phi=\frac{1}{\sqrt{2}}(\varphi_1+\i\varphi_2)$.
\begin{align*}
\mathscr{L}&= -\frac{1}{2\xi}(\partial_{\mu}A^{\mu})^2-\frac{1}{4}F_{\mu\nu}^2+\frac{1}{2}[D_{\mu}(\varphi_1+\i\varphi_2)][D^{\mu\star}(\varphi_1-\i\varphi_2)] \qquad |F_{\mu\nu}^2\equiv F^{\mu\nu}F_{\mu\nu} \\
&= -\frac{1}{2\xi}(\partial_{\mu}A^{\mu})^2-\frac{1}{4}F_{\mu\nu}^2+\frac{1}{2}[(\partial_{\mu}+ieA_{\mu})(\varphi_1+\i\varphi_2)][(\partial^{\mu}-ieA^{\mu})(\varphi_1-\i\varphi_2)] \\
&= -\frac{1}{2\xi}(\partial_{\mu}A^{\mu})^2-\frac{1}{4}F_{\mu\nu}^2+\frac{1}{2}(\partial_{\mu}\varphi_1)^2+\frac{1}{2}(\partial_{\mu}\varphi_2)^2+\frac{\i e}{2}A_{\mu}(\varphi_1+\i\varphi_2)\partial^{\mu}(\varphi_1-\i\varphi_2) \\
&\quad-\frac{ie}{2}\partial_{\mu}(\varphi_1+\i\varphi_2)A^{\mu}(\varphi_1+\i\varphi_2)+\frac{e^2}{2} A_{\mu}^2(\varphi_1^2+\varphi_2^2) \\
&= -\frac{1}{2\xi}(\partial_{\mu}A^{\mu})^2-\frac{1}{4}F_{\mu\nu}^2+\frac{1}{2} [(\partial_{\mu} \varphi_1)^2+(\partial_{\mu} \varphi_2)^2]+\cancel{\frac{\i e}{2} A_{\mu}\varphi_{1,2}\partial^{\mu}\varphi_{1,2}}-\cancel{\frac{\i e}{2}\partial_{\mu}\varphi_{1,2}A^{\mu}\varphi_{1,2}} \\
&\quad+\frac{e}{2}A_{\mu}\varphi_1\partial^{\mu}\varphi_2-\frac{e}{2}A_{\mu}\varphi_2\partial^{\mu}\varphi_1-\frac{e}{2}\partial_{\mu}\varphi_1 A^{\mu}\varphi_2+\frac{e}{2}\partial_{\mu}\varphi_2A^{\mu}\varphi_1+\frac{e^2}{2}A_{\mu}^2(\varphi_1^2+\varphi_2^2) \\
&= -\frac{1}{2\xi}(\partial_{\mu}A^{\mu})^2-\frac{1}{4}F_{\mu\nu}^2+\frac{1}{2}\sum_{i=1}^{2}(\partial_{\mu}\varphi_{i})^2+eA_{\mu} \varphi_1\accentset{\leftrightarrow}{\partial}^{\mu}\varphi_2 +\frac{1}{2}\sum_{i=1}^{2}e^2A_{\mu}^2\varphi_i^2 
\end{align*} 
Where we defined $\varphi_1\accentset{\leftrightarrow}{\partial}^{\mu}\varphi_2=\varphi_1(\partial^{\mu}\varphi_2)-(\partial^{\mu}\varphi_1)\varphi_2 $.\\\\
We follow the argumentation from the lecture and we redefine our field as follows:
\begin{equation*}
\zeta \equiv \zeta_{cl}+\hat{\zeta} \quad\text{for}\quad \zeta \in \{\varphi_1,\varphi_2 \}
\end{equation*}
That redifinition works, because $\zeta_{cl}$ is constant and the expectation value of $\hat{\zeta}$ vanishes. We can see that in the following:
\begin{align*}
\braket{\Omega|\hat{\zeta}|\Omega}&=\braket{\Omega|\zeta-\zeta_{cl}|\Omega}\\
&=\braket{\Omega|\zeta|\Omega}-\braket{\Omega|\zeta_{cl}|\Omega} \\
&=\zeta_{cl}-\zeta_{cl}\braket{\Omega|\Omega} \qquad &&\bigg|\braket{\Omega|\Omega}=1 \\
&=0
\end{align*}
We now from Poincar\'{e} symmetry that $\varphi_{cl}$ is constant, since it is the expectation value $\braket{\varphi}=\varphi_{cl}$. With this in mind our Lagrangian becomes:
\begin{equation*}
\mathscr{L}=\frac{1}{2}A_{\mu}[g^{\mu\nu}(\partial^2+e^2\varphi_{a}^2)+(\xi^{-1}-1)\partial^{\mu}\partial^{\nu}]A_{\nu}+\epsilon_{ab}eA_{\mu}\varphi_a\partial^{\mu}\varphi_b+\frac{1}{2}\varphi_a(-\partial^2)\varphi_a
\end{equation*}
Next we use our shift from above and set $\varphi_a\rightarrow \varphi_{cl,a}+\hat{\varphi}_{a}$ and plug it in our Lagrangian. We just need propagators for the effective potential, so we can drop all cubic and higher terms. We end up with the following Lagrangian:
\begin{equation*}
\mathscr{L}=\frac{1}{2}A_{\mu}[\underbrace{g^{\mu\nu}(\partial^{2}+e^2\varphi_{cl}^2)+(\xi^{-1}-1) \partial^{\mu} \partial^{\nu}}_{\i (\Delta^{-1})^{\mu\nu}}] A_{\nu}+A_{\mu} \underbrace{\epsilon_{ab}e\varphi_{cl, a}\partial^{\mu}}_{M^{\mu}_{b}} \hat{\varphi}_{b}+\frac{1}{2}\hat{\varphi_a}(\underbrace{-\partial^2}_{\i D^{-1}})\hat{\varphi}_{a} + \mathcal{O}(\hat{\varphi})
\end{equation*}
Where we defined the matrices connecting the fields as follows:
\begin{align*}
\i (\Delta^{-1})^{\mu\nu}&=g^{\mu\nu}(\partial^2+e^2\varphi_{cl}^2)+(\xi^{-1}-1)\partial^{\mu}\partial^{\nu} \\
M^{\mu}_{b}&=\epsilon_{ab}e\varphi_{cl,a}\partial^{\mu} \\
\i D^{-1}&=-\partial^2
\end{align*}
We want to bring our Lagrangian in a quadratic form, since the path integral of a quadratic form can be treated like a Gaussian, which makes our integration easier. We use the following shift:
\begin{equation*}
\hat{\varphi}_a\rightarrow \hat{\varphi}_a-\i DM_a^{\,\mu}A_{\mu}
\end{equation*}
When we plug our shift into our Lagrangian we get the result:
\begin{align*}
\mathscr{L}&=\frac{1}{2}A_{\mu}\i(\Delta^{-1})^{\mu\nu}A_{\nu}+A_{\mu}M^{\mu}_a(\hat{\varphi}_a-\i DM_a^{\,\nu}A_{\nu})+\frac{1}{2}[(\hat{\varphi}_a-\i DM_a^{\,\mu}A_{\mu})\i D^{-1}(\hat{\varphi}_a-\i DM_a^{\, \nu}A_{\nu})] \\
&=\frac{1}{2}A_{\mu}[\i(\Delta^{-1})^{\mu\nu}+\i DM_a^{\,\mu}M_a^{\,\nu}]A_{\nu}+\frac{1}{2}\hat{\varphi}_a \i D^{-1}\hat{\varphi}_a
\end{align*}
Now we have a quadratic form in $\hat{\varphi}_a$ and in $A$ individually. In the beginning we put our field in a expression as $\varphi\equiv \varphi_{cl}+\hat{\varphi}$ which changed the argument in our action as follows $S[\varphi_{cl}+\hat{\varphi}]$. The good thing about this form is that we can do a Taylor expansion around the $\varphi_{cl}$. That looks like:
\begin{equation*}
S[\varphi_{cl}+\hat{\varphi}]=S[\varphi_{cl}]+S^{\prime}[\varphi_{cl}]\hat{\varphi}+\frac{1}{2}S^{\prime\prime}[\varphi_{cl}]\hat{\varphi}^2+ \mathcal{O}(\hat{\varphi}^{3})
\end{equation*} 
Here $\varphi_{cl}$ are our external fields and $\hat{\varphi}$ our internal propagating fields. For 1-loop we can see that terms like $\hat{\varphi}^4$, $\varphi_{cl}\hat{\varphi}^3$ and $\varphi_{cl}^3\hat{\varphi}$ will not be relevant, since there is no way to draw diagrams with these vertices with only  $\varphi_{cl}$ as external $\hat{\varphi}$ as internal propagating fields. In our expansion we know that $\varphi_{cl}$ is the solution of the Euler-Lagrange equation and therefore it minimises the action. Following this argumentation the first derivative in $\varphi_{cl}$ has to be zero then it fulfills the minimise condition. To calculate the effective action we have to know the following:
\begin{equation*}
\exp{\i \Gamma[\varphi_{cl}]}=\int{\mathcal{D}\hat{\varphi}}\mathcal{D}A\,\exp{\i S[\varphi_{cl}+\hat{\varphi}]}
\end{equation*}
And we know that $\varphi_{cl}$ is constant so the kinetic terms will vanish in our Lagrangian. We can say:
\begin{align*}
\mathscr{L}[\varphi_{cl}]=\mathscr{L}_{kin}[\varphi_{cl}]-V[\varphi_{cl}] && \bigg|\mathscr{L}_{kin}[\varphi_{cl}]=0 
\end{align*}
Now we use the Taylor series of $S[\varphi_{cl}+\hat{\varphi}]$ and rewrite our path integral in terms of $S[\varphi_{cl}]$ and $S^{\prime\prime}[\varphi_{cl}]$. We just take our 1–loop correction and get:
\begin{equation*}
\exp{\i\Gamma[\varphi_{cl}]}=\int{\mathcal{D}\hat{\varphi}\mathcal{D}A\, \exp{\i\frac{1}{2}\hat{\varphi}S^{\prime\prime}[\varphi_{cl}]\hat{\varphi}}}
\end{equation*}
In our case we end up with:
\begin{align*}
\Gamma^{(1)}[\varphi_{cl}]&=\frac{1}{2}\i\hbar \ln{\big[\det(\i D^{-1})\det(\i(\Delta^{-1})^{\mu\nu}+\i DM_a^{\mu}M_b^{\nu})\big]} \\
&=\frac{1}{2}\i\hbar\ln{\big[\det(\i D^{-1})]}+\frac{1}{2}\i\hbar\ln{[\det(\i(\Delta^{-1})^{\mu\nu}+\i DM_a^{\,\mu}M_b^{\,\nu})\big]}
\end{align*}
We know that $\Gamma^{(1)}[\varphi_{cl}]$ is an action and an action has the form $\int{d^4x \mathscr{L}}$, where the effective potential is the potential of the classical field in the Lagrangian. We saw that in the calculation above. We rewrite the integral of the action $\Gamma$ in Fourier space to calculate the determinant. In Fourier space our derivative becomes $\partial=ik$, which makes things easier. The effective potential is only  
\begin{equation*}
V^{(1)}_\text{eff}=-\frac{1}{2}\i \hbar\int{\frac{d^4k}{(2\pi)^4}\ln{[\det(k^2)]}}-\frac{1}{2}\i\hbar\int{\frac{d^4k}{(2\pi)^4}\ln{\bigg[\det(\i (\tilde{\Delta}^{-1})^{\mu\nu}+\i\tilde{D}\tilde{M}_a^{\,\mu}\tilde{M}_b^{\,\nu})\bigg]}}
\end{equation*}
The first term is constant and independent of $\varphi_{cl}$. So we just have to calculate the second one.
\begin{equation*}
V^{(1)}_\text{eff}=-\frac{1}{2}\i\hbar\int{\frac{d^4k}{(2\pi)^4}\ln{\bigg[\det \big(\underbrace{\i( \tilde{\Delta}^{-1})^{\mu\nu}+\i\tilde{D}\tilde{M}_a^{\,\mu}\tilde{M}_b^{\,\nu}}_{\equiv N^{\mu \nu}}\big)\bigg]}}
\end{equation*}
We summarize the argument in the determinant in the way of:
\begin{align*}
\i(\tilde{\Delta}^{-1})^{\mu\nu}&=-k^2g^{\mu\nu}+k^{\mu}k^{\nu}+e^2\varphi_{cl}^2g^{\mu\nu}-\frac{1}{\xi}k^{\mu}k^{\nu} \\
\i\tilde{D}\tilde{M}_a^{\, \mu}\tilde{M}_a^{\,\nu}&=k^{-2}e^2\varphi_{cl}^2k^{\mu}k^{\nu} \\
N^{\mu\nu}&= \i(\tilde{\Delta}^{-1})^{\mu\nu} + \i\tilde{D}\tilde{M}_a^{\, \mu}\tilde{M}_a^{\,\nu} \\
&\equiv a g^{\mu\nu}+bk^{\mu}k^{\nu}
\end{align*}
The goal is to calculate the determinant of $N^{\mu\nu}$. Which could be done with the following:
\begin{align*}
\det(ag^{\mu\nu}+bk^{\mu}k^{\nu})&=a^4\det(g^{\mu\nu}+\frac{b}{a}k^{\mu}k^{\nu}) \quad &&\bigg|e^{\frac{b}{a}k^{\mu}k^{\nu}}\approx \delta^{\mu\nu}+\frac{b}{a}k^{\mu}k^{\nu} \\
&=-a^4\det(e^{\frac{b}{a}k^{\mu}k^{\nu}})=a^4e^{Tr[\frac{b}{a}k^{\mu}k^{\nu}]} \\
&=-a^4e^{\frac{b}{a}k^2} \quad \text{back to } 1^\text{st} \text{ order} \\
&=-a^4 \bigg(1+\frac{b}{a}k^2 \bigg)=-a^3(a+bk^2)
\end{align*}
Our effective potential for 1-loop diagrams is:
\begin{equation*}
V^{(1)}_\text{eff}=-\frac{1}{2}\i\hbar\int{\frac{d^4k}{(2\pi)^4}\ln{[-a^3(a+bk^2)]}}
\end{equation*}

 

\end{addmargin}
\noindent\rule{\textwidth}{1pt}
(see also \url{https://doi.org/10.1103/PhysRevD.9.1686})
\end{document}



