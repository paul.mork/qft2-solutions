\documentclass[10pt]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage[a4paper,width=150mm,top=25mm,bottom=25mm]{geometry}
\usepackage{enumerate} %Roman enumeration
\usepackage{dsfont}
\usepackage{sectsty}
\usepackage{mathtools}
\usepackage{float}
\usepackage{physics}
\usepackage{mathrsfs}
\usepackage{braket}
\usepackage{cancel}
\usepackage{slashed}
\usepackage{accents}
\usepackage{pdfpages}
\renewcommand*{\thepage}{\large\arabic{page}}

\allsectionsfont{\centering}
  
% New commands
\newcommand*{\QED}{\hfill\ensuremath{\blacksquare}}%
\newcommand*{\QEDB}{\hfill\ensuremath{\square}}%
\renewcommand*{\i}{\mathrm{i}}%
\newcommand*{\allimplies}{\big\} \hspace*{-0.2cm} \implies }%
\newcommand\hteq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny !}}}{=}}}
\newcommand\oneeq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny (i)}}}{=}}}
\newcommand\indeq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny ind}}}{=}}}
\newcommand\stareq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny \ref{eq-Lemma_1}}}}{=}}}


\begin{document}
% Original sheet 10
\includepdf[pages=-]{sheet10.pdf}
% Exercise 1
\noindent\rule{\textwidth}{2pt}
\textbf{Exercise 1. Scalar electrodynamics with SSB -- Solution:}\\\
\begin{addmargin}[1cm]{0cm}
%Text here
In this exercise we consider the abelian Higgs model with the following classical Lagrangian:
\begin{align*}
\mathscr{L}&=-\frac{1}{4}F_{\mu\nu}F^{\mu\nu}+(\partial_{\mu}+ieA_{\mu})\Phi^{\dagger}(\partial^{\mu}-ieA^{\mu})\Phi-\frac{\lambda}{4}(\Phi^{\dagger}\Phi-\frac{\mu^{2}}{2\lambda})^2\\
&=-\frac{1}{4}F_{\mu\nu}F^{\mu\nu}+\underbrace{|(\partial_{\mu}+ieA_{\mu})\Phi)|^2}_{\mathscr{L}_1}\underbrace{-\frac{\lambda}{4}(\Phi^{\dagger}\Phi-\frac{\mu^2}{2\lambda})^2}_{\mathscr{L}_2}
\end{align*}
We use the second equality of the Lagrangian, because it allows us to use the following formalism:
\begin{equation*}
|\kappa|^2=[\mathfrak{R} ( \kappa )^2+\mathfrak{I} ( \kappa)^2] \text{     ,}\quad|\kappa=\mathfrak{R}(\kappa)+i\mathfrak{I}(\kappa)
\end{equation*}
We use the hint in the exercise and we write the complex scalar field as 
\begin{equation*}
\Phi=\frac{1}{\sqrt{2}}\left (\frac{\mu}{\sqrt{\lambda}}+\phi_1+i\phi_2\right ) \quad |\mu,\lambda\in \mathbb{R} 
\end{equation*}
Now we plug the complex scalar field in $\mathscr{L}_1$ and $\mathscr{L}_2$. Start with evaluating the first:
\begin{align*}
\mathscr{L}_1&=\frac{1}{2}|\partial_{\mu}\phi_1+i\partial_{\mu}\phi_2+ieA_{\mu}\frac{\mu}{\sqrt{\lambda}}+ieA_{\mu}\phi_1-eA_{\mu}\phi_2|^2 \\
&=\frac{1}{2}\left( (\partial_{\mu}\phi_1-eA_{\mu}\phi_2)^2+(\partial_{\mu}\phi_2+eA_{\mu}(\frac{\mu}{\sqrt{\lambda}}+\phi_1))^2 \right) \\
&=\underbrace{\frac{1}{2}\sum_{i=1}^{2}{(\partial_{\mu}\phi_i)^2}}_{\text{Kin. term}}+\underbrace{\frac{e^2}{2}\sum_{i=1}^{2}{(A_{\mu}\phi_i)^2}+eA_{\mu}\phi_1\accentset{\leftrightarrow}{\partial}^{\mu}\phi_2 +\frac{e^2\mu}{\sqrt{\lambda}}A_{\mu}^2\phi_1}_{\text{Vertices}} +\underbrace{\frac{e^2\mu^2}{2\lambda}A_{\mu}^2}_{\text{Photon mass term}}+\underbrace{\frac{e\mu}{\sqrt{\lambda}}A_{\mu}\partial^{\mu}\phi_2}_{\text{Boson mixing}}
\end{align*} 
Now we evaluate the second term:
\begin{align*}
\mathscr{L}_2&=-\frac{\lambda}{4}\left( \frac{1}{2}(\frac{\mu}{\sqrt{\lambda}}+\phi_1-i\phi_2)(\frac{\mu}{\sqrt{\lambda}}+\phi_1+i\phi_2)-\frac{\mu^2}{2\lambda} \right)^2 \\
&=-\frac{\lambda}{16}\left( \bcancel{\frac{\mu^2}{\lambda}}+\frac{\mu}{\sqrt{\lambda}}\phi_1+\bcancel{i\frac{\mu}{\sqrt{\lambda}}\phi_2}+\frac{\mu}{\sqrt{\lambda}}\phi_1+\phi_1^2+\bcancel{i\phi_1\phi_2}-i\bcancel{\frac{\mu}{\sqrt{\lambda}}\phi_2}-\bcancel{i\phi_1\phi_2}+\phi_2^2-\bcancel{\frac{\mu^2}{\lambda}} \right)^2 \\
&=-\frac{\lambda}{16} \left( 4\frac{\mu^2}{\lambda}\phi_1^2+4\frac{\mu}{\sqrt{\lambda}}\phi_1^3+4\frac{\mu}{\sqrt{\lambda}}\phi_1\phi_2^2+\phi_1^4+2\phi_1^2\phi_2^2+\phi_2^4 \right) \\
&=\underbrace{-\frac{\mu^2}{4}\phi_1^2}_{\text{Higgs mass term}}\underbrace{-\frac{1}{4}\sqrt{\lambda}\mu\phi_1^3-\frac{1}{4}\sqrt{\lambda}\mu \phi_1\phi_2^2-\frac{\lambda}{16}\phi_1^4-\frac{\lambda}{8}\phi_1^2\phi_2^2-\frac{\lambda}{16}}_{\text{3- and 4-boson vertices}}
\end{align*}
Now we introduce the Gauge fixing and Faddev-Popov term. Therefore we go to the functional integral
\begin{equation*}
Z=\int{\mathcal{DA}\mathcal{D}\Phi\,\,e^{i\int{\mathscr{L}[A,\Phi]}}}
\end{equation*}
and we insert a $\mathds{1}$, which keeps our functional invariant. We use the Faddeev-Popov gauge fixing:
\begin{subequations}
\begin{align*}
\Delta_{FP}\Delta_{FP}^{-1}&=\mathds{1} \\
\Delta_{FP}^{-1}&=\int{\mathcal{D}\epsilon\delta(G({}^{\epsilon}A))} \\
\Delta_{FP}&=det(\frac{\delta G({}^{\epsilon}A) }{\delta \epsilon})
\end{align*}
\end{subequations}
Now we plug that in our functional integral and we find:
\begin{equation*}
Z=C\cdot \int{\mathcal{DA}\mathcal{D}\Phi \,\, e^{i\int{\mathscr{L}}}}\delta(G({}^{\epsilon}A))\frac{\delta(G({}^{\epsilon }A))}{\delta\epsilon} 
\end{equation*}
We take the gauge fixing constraint $\delta(G(x)-\omega(x))$ and integrate over $\omega(x)$ with a Gaussian weight. This yields:
\begin{equation*}
Z=C^{\prime}\cdot \int{\mathcal{DA}\mathcal{D}\Phi e^{i\int{d^4x(\mathscr{L}-\frac{1}{2}(G)^2)}}det(\frac{\delta G}{\delta \epsilon})}
\end{equation*}
A special choice of the gauge-fixing function is 
\begin{equation*}
G=\frac{1}{\sqrt{\xi}}\left( \partial_{\mu}A^{\mu}-\xi e \frac{\mu}{\sqrt{\lambda}}\phi_2 \right)
\end{equation*}
And the gauge-fixing Lagrangian is 
\begin{equation*}
\mathscr{L}_{GF}=-\frac{1}{2\xi}(\partial_{\mu}A^{\mu}-\xi e \frac{\mu}{\sqrt{\lambda}}\phi_2)^2
\end{equation*}
The fact that this mass is gauge dependent is a signal that the Goldstone boson is a fictitious field. We want to complete the Faddeev-Popov and we know that the Lagrangian depends on the gauge variation of G, which can be computed as follows:
\begin{equation*}
\frac{\delta G}{\delta \epsilon}=\frac{1}{\sqrt{\epsilon}}(-\frac{1}{e}\partial^2-\xi\frac{\mu}{\sqrt{\lambda}}\phi_1)
\end{equation*}
The determinant of this operator can be accounted for by including a set of Faddeev-Popov ghosts with the Lagrangian:
\begin{equation*}
\mathscr{L}_{ghosts}=\overline{c}\left[ -\partial^2-\xi \frac{e\mu}{\sqrt{\lambda}}\phi_1 \right]c
\end{equation*}
The total Lagrangian therefore is:
\begin{align*}
\mathscr{L}&=\underbrace{-\frac{1}{4}F_{\mu\nu}F^{\mu\nu}+\frac{1}{2}\sum_{i=1}^{2}{(\partial_{\mu}\phi_i)^2}}_{\text{kinetic terms}} \\
&+\underbrace{\frac{1}{2}e^2\frac{\mu^2}{\lambda}A_{\mu}^2-\frac{1}{4}\mu^2\phi_1^2}_{\text{Photon and Higgs boson mass terms}}\\
&-\underbrace{\frac{1}{2}\xi \frac{e^2\mu^2}{\lambda}\phi_2^2}_{\text{Goldstone boson mass term}}\\
&-\underbrace{\frac{1}{2\xi}(\partial_{\mu}A^{\mu})^2+\overline{c}(-\partial^2-\xi \frac{e^2\mu^2}{\lambda}(1+\phi_1\frac{\sqrt{\lambda}}{\mu}))c}_{\text{Gauge fixing and Faddeev-Popov terms}}\\
&+\underbrace{\frac{e^2}{2}\sum_{i=1}^{2}{(A_{\mu}\phi_i)^2}+eA_{\mu}\phi_1\accentset{\leftrightarrow}{\partial}^{\mu}\phi_2}_{\text{vertices}}\\
&+\underbrace{\frac{e^2\mu}{\sqrt{\lambda}}(A_{\mu}^2)-\frac{1}{4}\sqrt{\lambda}\mu\phi_1^3-\frac{1}{4}\sqrt{\lambda}\phi_1\phi_2^2-\frac{\lambda}{16}\phi_1^4-\frac{\lambda}{8}\phi_1^2\phi_2^2-\frac{\lambda}{16}\phi_2^4}_{\text{vertices}}
\end{align*}




\end{addmargin}
\noindent\rule{\textwidth}{1pt}
\clearpage

% Exercise 2
\noindent\rule{\textwidth}{2pt}
\textbf{Standard Model with additional Higgs field -- Solution:}\\\
\begin{addmargin}[1cm]{0cm}
% Text here
a) We want to build our Lagrangian with the correct mass dimension for a complex scalar field $\phi_2$. The Lagrangian is the Higgs Lagrangian:
\begin{equation*}
\Delta\mathscr{L}^{Higgs}=|D_{\mu}\phi_2|^2+\mu^2\phi_2^{\dagger}\phi_2-\frac{\lambda}{4}|\phi_2|^4+C_1(\phi_1^{\dagger}\phi_2\phi_2^{\dagger}\phi_1)+C_2(\phi_1^{\dagger}\phi_1\phi_2^{\dagger}\phi_2)    \, , \quad \lambda,\mu^2>0
\end{equation*}
Here we use the hint from the sheet and we use the VEV:
\begin{equation*}
\braket{\phi_2}=\phi_{ground}= \frac{1}{\sqrt{2}}\left( \begin{array}{c}\tilde{v}e^{i\xi} \\ 0 \\ \end{array}\right)
\end{equation*}
The term which gives us the mass for the gauge bosons is the kinetical term
\begin{equation*}
|D_{\mu}\phi_2|^2 =\left| (\partial_{\mu}\mathds{1}_{2x2}-ig_2I^aW_{\mu}^a+\frac{i}{2}g_1YB_{\mu}\mathds{1}_{2x2})\phi_2 \right|^2\, ,\qquad I^a=\frac{\sigma^{a}}{2}
\end{equation*}
Now we plug everything in and we know that the hypercharge is $Y=-\frac{1}{2}$, therefore we have the following covariant derivative:
\begin{align*}
\Rightarrow |D_{\mu}\phi_2|^2 \bigg|_{\phi_{2} = \langle \phi_{2} \rangle} &=\left| \frac{1}{\sqrt{2}}\left(\begin{array}{cc} \frac{1}{\sqrt{2}}g_2W^3_{\mu}+\frac{g_1}{2\sqrt{2}}B_{\mu} & g_2W^{+}_{\mu}\\ g_2W^{-}_{\mu} & -\frac{1}{\sqrt{2}}g_2W^3_{\mu}+\frac{g_1}{2\sqrt{2}}B_{\mu} \end{array}\right) \frac{1}{\sqrt{2}}\left( \begin{array}{c}\tilde{v} \\ 0 \\ \end{array}\right) \right|^2 \\
\\
&=\bigg(\frac{1}{2}\tilde{v} \bigg)^2 \left| \left( \begin{array}{c} \frac{1}{\sqrt{2}}g_2W_{\mu}^3+\frac{g_1}{2\sqrt{2}}B_{\mu} \\ g_2W^{-}_{\mu} \\ \end{array}\right) \right|^2\\
\\
&= \underbrace{\bigg(\frac{g_2}{2}\tilde{v}\bigg)^2 W_{\mu}^{-}W^{\mu,+}+\frac{1}{8}\tilde{v}^2 \bigg(g_2W^{3}_{\mu}+g_1\frac{1}{2}B_{\mu} \bigg)^2}_{I_1}
\end{align*}
Since we know that the prefactor of $W^+W^-$ gives us the mass for the W-Boson, we have to consider the mixed part. The mass for the $W$-Boson is given by $m_W=\frac{g_2}{2}\tilde{v}$ only for the additional term. This mass has to be corrected by the other terms in the Lagrangian. We just have a part of the Lagrangian from which we determined the mass for the Boson. To consider the full mass we have to take also the Higgs Mass for the Hypercharge $Y=1$. From the lecture we know, that the covariant derivative on the field $v_1$ for the hypercharge $Y=1$ is given by:
\begin{equation*}
|D_{\mu}\phi_1|^{2}=\underbrace{\bigg(\frac{g_2}{2}v_1 \bigg)^2 W_{\mu}^{-}W^{\mu,+}+\frac{1}{8}v_1^2 \bigg(g_2W^{3}_{\mu}+g_1B_{\mu} \bigg)^2}_{I_2}
\end{equation*}
We add this with our additional term for $\phi_2$ and we end up with:
\begin{equation*}
I_1+I_2\equiv (\tilde{v}^2+v_1^2) \bigg(\frac{g_2}{2} \bigg)^2 W_{\mu}^{-}W^{\mu,+}+\frac{1}{8}\bigg[ (\tilde{v}+v_1^2)(g_2^2(W_{\mu}^{3})^2)+(\tilde{v}^2+2v_1^2)g_2W_{\mu}^3g_1B_{\mu}+g_1^2B_{\mu}^2 \bigg(\frac{1}{4}\tilde{v}+v_1^2 \bigg) \bigg]
\end{equation*}
Now we have to evaluate the Mass matrix, which we could do in the following way:
\begin{equation*}
\begin{pmatrix}
W^{3} & B
\end{pmatrix}
M^{2}
\begin{pmatrix}
W^{3} \\ B
\end{pmatrix}
\equiv M^{2}_{WW}(W^3)^2+M^{2}_{BB}B^2+2M^{2}_{BW}BW^{3}
\end{equation*}
The indices $i,j$ stands for the Bosons $W$ and $B$. The Matrix is then:
\begin{equation*}
M^{2}= \left(\begin{array}{cc} \frac{1}{8}(\tilde{v}^2+v_1^2)g_2^2 & \frac{1}{8}g_1g_2(v_1^2+\frac{1}{2}\tilde{v}^2)\\ \frac{1}{8}g_1g_2(v_1^2+\frac{1}{2}\tilde{v}^2)& \frac{1}{8}(\frac{1}{4}\tilde{v}^2+v_1^2)g_1^2\end{array}\right)
\end{equation*}
The determinant of the Mass matrix is not zero, so we have to calculate the eigenvalues. The Eigenvalue for the $Z$-Boson mass is:
\begin{equation*}
m_Z^2=\frac{g_1g_2}{16} \bigg[2v_1^2+\frac{3}{2}\tilde{v}^2+\sqrt{v_1^4+326v_1^2\tilde{v}^2+\frac{9}{4}\tilde{v}^4} \bigg]
\end{equation*}
The ratio of $m_Z$ and $m_W$ is obviously not conserved. There are no Fermions which couples in our theory with $\phi_2$ so the question for flavour is irrelevant. There is no Flavour-changing neutral current.

b) For the $\Delta$ particle it is the same calculation. It works analogues to the calculation before.
The difference to before is, that we have a Triplett instead of a dublett. The Hypercharge is also different $Y=1$. We start with the additional terms to our Lagrangian and we get:
\begin{equation*}
\delta\mathscr{L}^{Higgs}\equiv |D_{\mu}\Delta|^2+\mu^2\Delta\Delta^{\dagger}-\frac{\lambda}{4}|\Delta|^4+C(\Phi\Phi^{\dagger}\Delta\Delta^{\dagger})
\end{equation*}
The term which gives the Mass to the $\Delta$ is the kinetical term. The covariant derivative reads as follows:
\begin{equation*}
|D_{\mu}\Delta|^2=\left| (-ig_2\frac{\tau^{a}}{2}W_{\mu}^{a}+ig_1\frac{1}{2}B_{\mu}\mathds{1}_{3x3})\frac{1}{\sqrt{2}} \left( \begin{array}{c} 0 \\\ 0 \\\ \omega e^{i\xi} \end{array} \right) \right|^2
\end{equation*}
We can simply see that the ratio is conserved. And $\Delta$ is a triplet, so we do not have coupling to fermions and no FCNC either.


\end{addmargin}
\noindent\rule{\textwidth}{1pt}
\clearpage

\end{document}

