\documentclass[10pt]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage[a4paper,width=150mm,top=25mm,bottom=25mm]{geometry}
\usepackage{enumerate} %Roman enumeration
\usepackage{dsfont}
\usepackage{sectsty}
\usepackage{mathtools}
\usepackage{float}
\usepackage{physics}
\usepackage{pdfpages}
\usepackage{mathrsfs}


\allsectionsfont{\centering}

% New commands
\newcommand*{\QED}{\hfill\ensuremath{\blacksquare}}%
\newcommand*{\QEDB}{\hfill\ensuremath{\square}}%
\renewcommand*{\i}{\mathrm{i}}%
\newcommand*{\allimplies}{\big\} \hspace*{-0.2cm} \implies }%
\newcommand\hteq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny !}}}{=}}}
\newcommand\oneeq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny (i)}}}{=}}}
\newcommand\indeq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny ind}}}{=}}}
\newcommand\stareq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny \ref{eq-Lemma_1}}}}{=}}}


\begin{document}
% Original sheet 4
\includepdf[pages=-]{sheet1.pdf}
% Exercise 1
\noindent\rule{\textwidth}{2pt}
%\subsection*{Exercise 1: Gauge groups}
%Gauge groups play an important role in particle physics and therefore it is useful to examine their general properties.
%\begin{enumerate}[(i)]
%\item{Derive the dimensions of the $SU(N)$, $SO(N)$ and $Sp(2N)$ gauge groups.}.
%\item{Work out the following expressions
%\begin{align*}
%SU(N) : (T^a)_{ij} (T^a)_{kl} &= T_F \bigg( \delta_{il} \delta_{jk} - \dfrac{1}{N} \delta_{ij} \delta_{kl} \bigg) \\
%SO(N) : (T^a)_{ij} (T^a)_{kl} &= \dfrac{1}{2} T_F \bigg( \delta_{il} \delta_{jk} - \delta_{ik} \delta_{jl} \bigg)\\
%Sp(2N) : (T^a)_{ij} (T^a)_{kl} &= \dfrac{1}{2} T_F \bigg( \delta_{il} \delta_{jk} - \epsilon_{ik} \epsilon_{jl} \bigg)
%\end{align*}
%with the generators $T^a$ of a real Lie algebra and the normalization constant $T_F$ defined through:
%\begin{equation*}
%\Tr (T^a T^b) = T_F \delta^{ab}
%\end{equation*}
%}
%\item{Calculate the expression
%\begin{equation*}
%if^{abc} = \dfrac{1}{T_F} \Tr \big( T^a T^b T^c -T^{c} T^{b} T^{a} \big)
%\end{equation*}
%where $T^a$ are generators for the Lie group $SU(N)$, $f^{abc}$ are the structure functions and $T_F$ is the normalization constant. Those are defined through the basic relations $[T^a,T^b] = i f^{abc} T^c$ and the normalization relation from above. Additionally, compute the corresponding identities the groups $SO(N)$ and $Sp(2N)$. 
%}
%\item{Determine the Casimir operators $C_F$ and $C_A$ for the groups $SU(N)$, $SO(N)$ and $Sp(2N)$. Furthermore compute
%\begin{align*}
%\Tr (T^a T^b T^a T^b) \\
%\Tr (T^a T^b T^c) i f^{abc}
%\end{align*}
%only for $SU(N)$.
%}
%\end{enumerate}
%\noindent\rule{\textwidth}{1pt}
\textbf{Exercise 1 -- Solution:}\\\
\begin{addmargin}[1cm]{0cm}

\textit{Ad (i):} To compute the dimensions of the Lie groups one can look at the generators spanning the Lie algebra.
\begin{itemize}
\item{$SO(N)$: The generators $T^a$ of $\mathfrak{so}(N)$ are real antisymmetric $N \times N$ matrices. Therefore all entries above the diagonal are freely choosable. \\
$\allimplies \dim \mathfrak{so}(N) = \dim SO(N) = \frac{N(N-1)}{2}$ 
}
\item{$U(N)$: Before we compute $\dim SU(N)$ it is easier to first consider $U(N)$. We therefore again take a look at the associated Lie algebra $\mathfrak{u}(N)$. The generators of $\mathfrak{u}(N)$ are given by antihermitean $N \times N$ matrices. Therefore again all entries above the diagonal are freely choosable, but have to be counted twice since the entries are complex numbers. The diagonal has to be purely imaginary and therefore contributes a summand $N$ to the dimension. \\
$\allimplies \dim \mathfrak{u}(N) = \dim U(N) = N + 2 \cdot \dfrac{N(N-1)}{2} = N^2$ 
}
\item{$SU(N)$: The condition $\Tr(T^a) = 0$ gives one restraint, thereby we get:  \\
$\allimplies \dim \mathfrak{su}(N) = \dim SU(N) = N^2 - 1$
}
\item{$Sp(2N)$: We start by deriving the restraints on the generators of $\mathfrak{sp}(2N)$. The Lie group $Sp(2N)$ is given by:
\begin{equation*}
Sp(2N) := \{ A \in \text{GL}(2N, \mathbb{R}) | A^\top \epsilon A = \epsilon \} 
\quad \text{where} \quad 
\epsilon := \begin{pmatrix}
0 & \mathds{1} \\
- \mathds{1} & 0 \\
\end{pmatrix}
\end{equation*} 
We then can write $A \in Sp(2N)$ as $A=\exp \{ i R \}$ for $R \in \mathfrak{sp}(2N)$. The condition then yields on the Lie algebra level:
\begin{equation*}
e^{iR^\top} \epsilon \, e^{iR} \sim (\mathds{1} + i R^\top) \epsilon (\mathds{1} + i R) = \epsilon + i (R^\top \epsilon + \epsilon R)  \hteq \epsilon \\ 
\end{equation*}
\begin{equation*}\label{eq-sp}
\allimplies R^\top \epsilon + \epsilon R = 0 \tag{$\ast$}
\end{equation*}
Thereby the generators have to fulfill the condition $R^\top \epsilon + \epsilon R$. Consider a general element $R \in \mathfrak{sp}(2N)$ given as:
\begin{equation*}
R = \begin{pmatrix}
A & B \\ 
C & D \\
\end{pmatrix}
\end{equation*}
Inserting $R$ in (\ref{eq-sp}) yields:
\begin{equation*}
\begin{pmatrix}
C - C^\top & A^\top + D \\
-(A + D^\top) & B^\top-B \\
\end{pmatrix} = 0
\end{equation*}
Such that $B$ and $C$ have to be symmetric and $A$ completely determines $D$.
\begin{itemize}
\item{$A$ is arbitrary, which corresponds to a summand $N^2$.
}
\item{$B$ and $C$ are symmetric. Therefore we have an extra summand $2 \cdot \frac{N(N+1)}{2}$.
}
\end{itemize}
$\allimplies  \dim \mathfrak{sp}(2N) = \dim Sp(2N) = N^2 + N(N+1) = N(2N+1)$
}
\end{itemize}
\textit{Ad (ii):} For $SU(N)$ and $SO(N)$ we can use the completeness relation:
\begin{equation*}
X = \frac{1}{T_F} \Tr(X T^a) T^a \Leftrightarrow X_{ij} = \dfrac{1}{T_F} X_{mn} T^a_{nm} T^a_{ij}
\end{equation*}
Consider the matrices (with $k$ and $l$ fixed):
\begin{align*}
Y_{ij} &:= \dfrac{1}{2} (\delta_{il}  \delta_{jk} + \delta_{ik} \delta_{jl}) - \dfrac{1}{N} \delta_{kl} \delta_{ij} \\
Z_{ij} &:= \dfrac{\i}{2} (\delta_{il}  \delta_{jk} - \delta_{ik} \delta_{jl}) 
\end{align*}
We immediately notice that $Y,Z\in \mathfrak{su}(N)$, while $Z \in \mathfrak{so}(N)$. With the completeness relation we obtain:
\begin{align*}
Y_{ij} := \dfrac{1}{	2 T_F} (T^a_{kl} + T^a_{lk}) T^a_{ij} \\
Z_{ij} := \dfrac{\i}{2 T_F} (T^a_{kl} - T^a_{lk}) T^a_{ij}
\end{align*}
From which we can immediately conclude:
\begin{itemize}
\item{$SU(N): \quad T^a_{ij} T^a_{kl} = T_F (Y_{ij} - \i Z_{ij})$}
\item{$SO(N): \quad T^a_{ij} T^a_{kl} =  - \i T_F Z_{ij}$}
\end{itemize} 
For $SO(N)$ we used the fact that the generators are antisymmetric. The statement above gives exactly our claim. \QED \\\\
For $Sp(2N)$ on the other hand we have to use the following completeness relation:
\begin{align*}
\mathfrak{sp}(2N) \ni X &= \dfrac{1}{2} (W + \epsilon W^\top \epsilon), \qquad W^\dagger = W \\
X &= \frac{1}{T_{F}} \Tr ( X T^{a} ) T^{a} 
\end{align*}
With $W := \dfrac{A+A^\dagger}{2}$ and $W := \i \dfrac{A-A^\dagger}{2}$, where $A$ are arbitrary matrices, we obtain
\begin{align*}
\dfrac{1}{4} \big( (A+A^\dagger ) + \epsilon (A+A^\dagger)^\top \epsilon \big) &= \dfrac{1}{2 T_F} \Tr ( ( A+A^\dagger ) T^a ) T^a \\
\dfrac{\i}{4} \big( (A-A^\dagger ) + \epsilon (A-A^\dagger)^\top \epsilon \big) &= \dfrac{\i}{2 T_F} \Tr ( ( A-A^\dagger ) T^a ) T^a \\
\end{align*}
Here we used the fact that:
\begin{equation*}
\Tr (\epsilon W^{\top} \epsilon T^{a} ) = \Tr  \normalsize( W^{\top} \underbrace{\epsilon T^{a} \epsilon}_{{T^{a}}^{\top}} \normalsize) = \Tr (W T^{a}) 
\end{equation*}
From the above we conclude
\begin{equation*}
\implies \dfrac{1}{2} (A + \epsilon A^\top \epsilon ) = \dfrac{1}{T_F} \Tr (AT^a) T^a
\end{equation*}
Now we again insert a special matrix $A := \delta_{il} \delta_{jk}$ with $l$ and $k$ fixed such that we get:
\begin{equation*}
\allimplies T^a_{ij} T^a_{kl} = \dfrac{T_{F}}{2} (\delta_{il} \delta_{jk} - \epsilon_{ik} \epsilon_{jl})
\end{equation*} \QED \\\\
\textit{Ad (iii):} By the cyclicity of the trace we get:
\begin{equation*}
\Tr (T^a T^b T^c - T^c T^b T^a) = \Tr \normalsize(T^a \underbrace{[T^b,T^c]}_{\i f^{bcd} T^d} \normalsize)= \i f^{bcd} \underbrace{\Tr (T^a T^d)}_{T_F \delta^{ad}} = \i f^{bca} T_F 
\end{equation*}
This formula holds in general, but for $SO(N)$ and $Sp(2N)$ we can even simplify it. We can do this by using relations for the transpose:
\begin{equation*}
SO(N): \, T^a = -{T^a}^\top, \qquad Sp(2N): \, {T^a}^\top = - \epsilon T^a \epsilon^{-1}
\end{equation*}
Since the trace is invariant under transposition we get:
\begin{equation*}
\Tr (T^c T^b T^a) = \Tr \big({T^a}^\top {T^b}^\top {T^c}^\top \big) = - \Tr (T^a T^b T^c)
\end{equation*}
for both cases. We can then conclude:
\begin{equation*}
\i f^{abc} = \dfrac{2}{T_F} \Tr (T^a T^b T^c) 
\end{equation*}
\textit{Ad (iv):} We start by calculating the traces, i.e. we start with the following for $SU(N)$. 
\begin{equation*}
\Tr (T^a T^b T^a T^b) = T^a_{ij} T^b_{jk} T^a_{kl} T^b_{li} = {T_F}^2 \bigg(\dfrac{1}{N} - N \bigg)
\end{equation*}
Where we have used exercise 1 task \textit{(ii)} in the last equality. The next trace is
\begin{align*}
\Tr (T^a T^b T^c) \i f^{abc} &= \Tr \normalsize(T^a T^b \underbrace{\i f^{abc} T^c}_{[T^a,T^b]} \normalsize) = \Tr (T^a T^b T^a T^b) - \Tr \normalsize(T^a T^a \underbrace{T^b T^b}_{C_{F} \mathds{1}} \normalsize) \\
&={T_F}^2 \bigg( \dfrac{1}{N} - N \bigg) - {C_F}^2 N
\end{align*}
Lastly we calculate the Casmir operators in the fundamental and adjoint representation.
\begin{align*}
\Tr (T^a T^a) &= T_F \delta^{aa} = T_F d_A \hteq C_F d_F \implies C_F = \dfrac{T_F d_A}{d_F} \\
\Tr (T^a_{A} T^a_{A}) &= \Tr \big(C_A \mathds{1}_{d_A \times d_A} \big) = C_A d_A = (T^a_{A})_{bc} (T^a_{A})_{cb} = \i f^{bac} \i f^{cab}
\end{align*}
From the first equation we can immediately conclude all $C_{F}$ since we know $d_{F}=N$ and $d_{A}= \dim \mathfrak{g}$ from Task 1 \textit{(i)}, i.e.:
\begin{align*}
C_{F}^{SU(N)} = T_{F} \frac{N^{2}-1}{N}, \qquad C_{F}^{SO(N)} =T_{F} \frac{N-1}{2}, \qquad C_{F}^{Sp(2N)} = \frac{T_{F}}{2} (2N+1)
\end{align*}
To derive $C_{A}$ we need to simplify equation two.
\begin{equation*}
\i f^{bac} \i f^{cab} = \i f^{bac} \dfrac{1}{T_F} \Tr (T^c T^a T^b - T^b T^a T^c) = - \i \dfrac{2}{T_F} f^{abc} \Tr (T^a T^b T^c)
\end{equation*}
For $SU(N)$ we have already calculated this expression, therefore:
\begin{equation*}
C_{A}^{SU(N)} = 2 T_{F} N
\end{equation*}
Next we need to calculate it for $SO(N)$.
\begin{align*}
\i f^{abc} \Tr (T^{a}T^{b} T^{c} ) = \Tr (T^{a} T^{b} T^{a} T^{b}) - \Tr (T^{a} T^{a} T^{b} T^{b})
\end{align*}
The first term can be calculated as follows:
\begin{align*}
\Tr (T^{a} T^{b} T^{a} T^{b}) &= T^{a}_{ij} T^{a}_{kl} T^{b}_{jk} T^{b}_{li} \\
&= \frac{1}{4} {T_{F}}^{2} (\delta_{il} \delta_{jk} - \delta_{ik} \delta_{jl}) (\delta_{ji} \delta_{kl} - \delta_{jl} \delta_{ki}) \\
&= \frac{1}{4} {T_{F}}^{2} d_{F} (d_{F} -1 )
\end{align*}
While the second term is just ${C_{F}}^{2} d_{F}$. In total this is:
\begin{equation*}
C_{A} = T_{F} (N-2)
\end{equation*}
Next we turn to the $Sp(2N)$ group. The calculations are similar to $SO(N)$, we only have to evaluate the first term with the rules from $Sp(2N)$, i.e.:
\begin{align*}
\Tr (T^{a} T^{b} T^{a} T^{b}) &= T^{a}_{ij} T^{a}_{kl} T^{b}_{jk} T^{b}_{li} \\
&= \frac{1}{4} {T_{F}}^{2} (\delta_{il} \delta_{jk} - \epsilon_{ik} \epsilon_{jl}) (\delta_{ji} \delta_{kl} - \epsilon_{jl} \epsilon_{ki}) \\
&= - \frac{1}{4} {T_{F}}^{2} d_{F} (d_{F} + 1 )
\end{align*}
This yields:
\begin{equation*}
C_{A}^{Sp(2N)} = 2 T_{F} (N+1)
\end{equation*}
Collecting all results we get:
\begin{table}[H]
\centering
\renewcommand{\arraystretch}{2.5}
\begin{tabular}{c|c|c}
Group & $C_A$ & $C_F$ \\
\hline
$SU(N)$ & $2 T_F N$ & $T_F \dfrac{N^2-1}{N}$ \\
$SO(N)$ & $T_F (N-2)$ & $T_F \dfrac{N-1}{2}$ \\
$Sp(2N)$ & $2 T_F (N+1)$ & $\dfrac{T_F}{2} (2N+1)$
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{table}
\end{addmargin}
\noindent\rule{\textwidth}{1pt}
\newpage
%\subsection*{Exercise 2: CP violating term in non-Abelian gauge theories}
%Proof that $\theta^{ab} F^a_{\mu \nu} \tilde{F}^{b \mu \nu}$ is a total derivative, where $\tilde{F}^{a \mu  \nu} := \dfrac{1}{2} \epsilon^{\mu \nu \rho \sigma} F^a_{\rho \sigma}$ and $\theta^{ab}$ is a matrix in color space in the adjoint representation. \\\\
\noindent\rule{\textwidth}{2pt}
\textbf{Exercise 2 -- Solution:}\\\
\begin{addmargin}[1cm]{0cm}
Firstly we need to look at $\theta^{ab}$ since for a total arbitrary matrix the expression would not be a total derivative. 
\begin{equation*}
\mathscr{L}_\theta := \theta^{ab} F^a_{\mu \nu} \tilde{F}^{b \mu \nu} = \dfrac{1}{2} \theta^{ab} \underbrace{F^a_{\mu \nu} F^b_{\rho \sigma} \epsilon^{\mu \nu \rho \sigma}}_{\mu  \nu \leftrightarrow \rho \sigma}
\end{equation*}
We can therefore see that $\theta^{ab}$ has to be symmetric in $a \leftrightarrow b$ to fulfill this in general. Next we do a gauge transformation of $F_{\mu \nu}$ in order to extract its transformation law:
\begin{align*}
U F_{\mu \nu} U^\dagger &\sim (1 + \i \omega^{a'} T^{a'} ) F_{\mu \nu} (1 - \i \omega^{a''} T^{a''}) = F_{\mu \nu} + \i \omega^{c}(T^c F_{\mu \nu} - F_{\mu \nu} T^c)  \\
&= F_{\mu \nu} + \i \omega^{c} (\underbrace{[T^c,T^a]}_{\i f^{cab} T^b} F^a_{\mu \nu})
\end{align*}
Therefore $F_{\mu \nu}$ transforms as
\begin{equation*}
F^{b \mu \nu} \overset{U (\cdot) U^\dagger}{\mapsto} F^{b \mu \nu} - \omega^a F^{c \mu \nu} f^{acb} 
\end{equation*}
Combining this with the Lagrangian yields
\begin{align*}
\theta^{ab} F_{\mu \nu}^{a} F_{\rho \sigma}^{b}  \overset{U (\cdot) U^\dagger}{\mapsto} &\theta^{ab} (F^a_{\mu \nu} - \omega^{c} F^d_{\mu \nu} f^{c d a}) (F^b_{\rho \sigma} - \omega^e F^f_{\rho \sigma} f^{efb}) \\
&\theta^{ab} F^a_{\mu \nu} F^b_{\rho \sigma} - \omega^c  \underbrace{\theta^{ab}(F^d_{\mu \nu} F^b_{\rho \sigma} f^{cda} + F^a_{\mu \nu} F^d_{\rho \sigma} f^{cdb})}_{\hteq 0}
\end{align*}
This can be further simplified to:
 \begin{align*}
\theta^{ab} F^a_{\mu \nu} F^d_{\rho \sigma} f^{cdb} \overset{a\leftrightarrow d}{=} \theta^{db} F^d_{\mu \nu} F^a_{\rho \sigma} f^{cab} \overset{a \leftrightarrow b}{=} \theta^{ad} F^d_{\mu \nu} F^b_{\rho \sigma} f^{cba}
 \end{align*}
Such that:
\begin{equation*}
\theta^{ab} f^{cda} + \theta^{ad} f^{cba} = 0
\end{equation*}
From there on follows:
\begin{equation*}
\implies \theta^{ab} f^{dca} = - \theta^{ad} f^{bca}
\end{equation*}
\begin{align*}
l.h.s. &= \theta^{ab} f^{dca} = - \i \theta^{ab} (T^c)_{da}  = - \i (T^c \theta)_{db} \\
r.h.s. &= -\i (\theta T^c)_{db}
\end{align*}
\begin{equation*}
\allimplies T^c \theta = \theta T^c \Leftrightarrow [T^c,\theta] = 0 \overset{\text{Schur's lemma}}{\implies} \theta^{ab} = C \delta^{ab}
\end{equation*}
for some constant $C$. This empowers us to only look at
\begin{equation*}
F^a_{\mu \nu} F^a_{\rho \sigma} \epsilon^{\mu \nu \rho \sigma} = (\partial_{\mu} A^a_{\nu} - \partial_{\nu} A^a_{\mu} + g f^{acd} A^c_{\mu} A^d_{\nu}) (\partial_{\rho} A^a_{\sigma} - \partial_{\sigma} A^a_{\rho } + g f^{aef} A^e_{\rho } A^f_{\sigma} ) \epsilon^{\mu \nu \rho \sigma} 
\end{equation*}
We get terms like:
\begin{equation*}
\partial_{\mu} A^a_{\nu} \partial_{\rho} A^a_{\sigma} = \partial_{\mu} (A^a_{\nu} \partial_{\rho } A^a_{\sigma}) - \underbrace{A^a_{\nu} \partial_{\mu} \partial_{\rho} A^a_{\sigma}}_{= 0}
\end{equation*}
Here the last term vanishes when contracted with $\epsilon^{\mu \nu \rho \sigma}$. Furthermore there are also terms like
\begin{equation*}
(\partial_{\mu} A^a_{\nu}) A^e_{\rho} A^f_{\sigma} f^{aef} \epsilon^{\mu \nu \rho \sigma} = \dfrac{1}{3} \partial_{\mu} (A^a_{\nu} A^e_{\rho} A^f_{\sigma} ) f^{aef} \epsilon^{\mu \nu \rho \sigma}
\end{equation*}
and
\begin{equation*}
f^{aef} f^{acd} A^c_{\mu} A^d_{\nu} A^e_{\rho} A^f_{\sigma} \epsilon^{\mu \nu \rho \sigma} = 0
\end{equation*}
This term vanishes by the Jacobi identity. We conclude that $\theta^{ab} F^a_{\mu \nu} \tilde{F}^{b \mu \nu}$ is a surface term.
 \ \\ \vspace{0pt} \QED
\end{addmargin}
\noindent\rule{\textwidth}{1pt}
%\subsection*{Exercise 3: Equations of motion an energy-momentum tensor}
%Starting from the Lagrangian
%\begin{equation*}
%\mathscr{L} := - \dfrac{1}{4} g^{ab} F^a_{\mu \nu} F^{b \mu \nu}
%\end{equation*}
%\begin{enumerate}[(i)]
%\item{derive the equations of motion and the energy-momentum tensor $T^{\mu \nu}$}
%\item{obtain a symmetric version $\Theta^{\mu \nu}$ of the energy-momentum tensor $T^{\mu \nu}$ and determine the energy density $\Theta^{00}$ in terms of the electric and magnetic fields defined as $E^{ai} := F^{ai0}$ and $B^{ai} := - \dfrac{1}{2} \epsilon^{ijk} F^{ajk}$.}
%\end{enumerate}
\newpage
\noindent\rule{\textwidth}{2pt}
\textbf{Exercise 3 -- Solution:}\\\
\begin{addmargin}[1cm]{0cm}

\textit{Ad (i):} We start by computing the parts needed for the equations of motion.
\begin{align*}
\pdv{\mathscr{L}}{A^e_{\rho }} = - g^{ab} F^{a\rho \nu } f^{edb} A^d_{\nu}, \qquad
\pdv{\mathscr{L}}{(\partial_{\sigma} A^e_{\rho} )} = - g^{ae} F^{a \sigma \rho}
\end{align*}
It follows:
\begin{align*}
\allimplies 0 &= g^{ae} \partial_{\sigma} F^{a \sigma \rho} - g^{ab} f^{edb} A^d_{\nu} F^{a \rho \nu } = (\partial_{\sigma} \delta^{ed} + f^{ebd} A^{b}_{\sigma}) g^{ad} F^{a \sigma \rho} \tag{EOM} \\ 
&=: D^{ed}_{\sigma} (g^{ad} F^{a \sigma \rho} ) 
\end{align*}
The EOM is needed to deduce the energy-momentum tensor (i.e. the Belinfante-Rosenfeld tensor)
\begin{align*}
T^{\mu \nu} &= \pdv{\mathscr{L}}{(\partial_{\mu} A^b_{\rho})} (\partial^{\nu} A^b_{\rho}) (\partial^{\nu} A^b_{\rho}) - g^{\mu \nu} \mathscr{L} \\
&= \dfrac{1}{4} g^{\mu \nu} g^{ab} F^{a \alpha \beta} F^b_{\alpha \beta} - g^{ab } F^{a \mu \rho } \partial^{\nu} A^b_{\rho}
\end{align*}
\textit{Ad (ii):} We consider the total derivative: 
\begin{equation*}
\partial_{\rho} (g^{ab} F^{a \mu \rho} A^{b \nu} ) \overset{\text{EOM}}{=} g^{ab} F^{a \mu \rho} (\partial_{\rho} A^{b \nu} - f^{cdb} A^{c \nu} A^d_{\rho})
\end{equation*}
We can use this derivative to obtain the Belinfante-Rosenfeld tensor $\Theta^{\mu \nu}$:
\begin{align*}
\Theta^{\mu \nu} &= T^{\mu \nu} + \partial_{\rho} (g^{ab} F^{a \mu \rho} A^{b \nu} ) \\
&= g^{ab} \bigg( \dfrac{1}{4} g^{\mu \nu} F^{a \alpha \beta} F^b_{\alpha \beta} - F^{a \mu \rho} {F^{b \nu}}_{\rho} \bigg) 
\end{align*}
This expression looks a lot like the Belinfante-Rosenfeld tensor from QED. We can therefore reuse the results of QFT I to obtain $\Theta^{00}$ in terms of electric and magnetic fields, i.e.:
\begin{equation*}
\Theta^{00} = \frac{1}{2} g^{ab} (\vec{E}^{a} \cdot \vec{E}^{b} + \vec{B}^{a} \cdot \vec{B}^{b} )
\end{equation*}
\end{addmargin}
\noindent\rule{\textwidth}{1pt}
\end{document}

