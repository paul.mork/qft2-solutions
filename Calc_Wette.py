# -*- coding: utf-8 -*-
"""
Created on Mon Aug 19 21:06:13 2019

@author: Frank
"""


import numpy as np

def toData(s):
    name, votesStr = s.split(':')
    votesVal = [float(v) for v in votesStr.split(',')]
    return (name, np.asarray(votesVal))

f = open("Klausuraufgabenwetten.txt","r")
D = dict([toData(l) for l in f.readlines()])

inExam = [1.3, 8.1, 9.1]

def toScore(vals):
    if len(vals) != 10:
        raise ValueError()
    return sum([10-n for n in range(10) if (vals[n] in inExam)])

D = dict([(k, toScore(D[k])) for k in D.keys()])

print('Auswertung:')
for k in D.keys():
    print('  %s:  \t%i' % (k, D[k]))

