\documentclass[10pt]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage[a4paper,width=150mm,top=25mm,bottom=25mm]{geometry}
\usepackage{enumerate} %Roman enumeration
\usepackage{dsfont}
\usepackage{sectsty}
\usepackage{mathtools}
\usepackage{float}
\usepackage{physics}
\usepackage{slashed}
\usepackage{pdfpages}
\usepackage{hyperref}
\renewcommand*{\thepage}{\large\arabic{page}}

\allsectionsfont{\centering}

% New commands
\newcommand*{\QED}{\hfill\ensuremath{\blacksquare}}%
\newcommand*{\QEDB}{\hfill\ensuremath{\square}}%
\renewcommand*{\i}{\mathrm{i}}%
\newcommand*{\allimplies}{\big\} \hspace*{-0.2cm} \implies }%
\newcommand\hteq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny !}}}{=}}}
\newcommand\oneeq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny (i)}}}{=}}}
\newcommand\psibar{\overline \psi}
\newcommand\del{\partial}
\newcommand\delslash{\slashed \del}
\renewcommand\comm[2]{\left[#1,\ #2\right]}
\renewcommand\acomm[2]{\left\{#1,\ #2\right\}}
\newcommand\cst{\text{cst.}}

\begin{document}
% Original sheet
\includepdf[pages=-]{sheet8.pdf}
% Exercise 1
\noindent\rule{\textwidth}{2pt}
\textbf{Exercise 1a) -- Solution:}\\\
\begin{addmargin}[1cm]{0cm}
We consider the chiral symmetry-transformation: $\psi_i \to \gamma_5 \psi_i,\ \psibar_i \to -\psibar_i \gamma^5$ and use $\left(\gamma^5\right)^2 = 1$ and $\acomm {\gamma^5}{\gamma^\mu} = 0$
\begin{align*}
\Rightarrow& \psibar_i \delslash \psi_i \to \psibar_i \delslash \psi_i && \text{conserves symmetry}\\
& m \psibar_i \psi_i \to -m \psibar_i \psi_i && \text{doesn't conserve symmetry}
\end{align*}
$\Rightarrow\ \mathcal L = \psibar_i \i \delslash \psi_i + \frac12 g^2 \left(\psibar_i \psi_i\right)^2 \to \mathcal L$ conserves chiral symmetry, but adding an additional massterm $\mathcal L_m = m \psibar_i \psi_i \to - \mathcal L_m$ would break it. I.e. $m=0$.\\
(\textit{It wasn't asked in the question but since it's part of Bolun's solution,}) we now want to obtain the mass-dimension of $\psi$ in $\text{QED}_2$:
\begin{align*}
&2 = \text{dim}(\mathcal L) \overset!= \text{dim}(\psibar_i \delslash \psi_i) = 2\text{dim}(\psi_i) + 1\\
&\Rightarrow \text{dim}(\psi_i) = \frac12\\
&\Rightarrow \text{$\mathcal L$ is renormalizable.}
\end{align*}
\end{addmargin}

\textbf{Exercise 1b) -- Solution:}\\\
\begin{addmargin}[1cm]{0cm}
We define $\mathcal L _\sigma \coloneqq \psibar_i \i \delslash \psi_i - \sigma \psibar_i \psi_i - \frac 1 {2g^2} \sigma^2$, where $\sigma$ denotes a new real scalar field.
\begin{align*}
\int \mathcal D \psibar \mathcal D \psi \mathcal D \sigma e^{i \int d^2x \mathcal L_\sigma} = \int \mathcal D \psibar \mathcal D \psi e^{i \int d^2x \psibar_i \i \delslash \psi_i}\int \mathcal D \sigma e^{i \int d^2x \left(- \sigma \psibar_i \psi_i - \frac 1 {2g^2} \sigma^2\right)}
\end{align*}
The argument in the second integral can be rewritten by completing the square:
\begin{align*}
- \sigma \psibar_i \psi_i -& \frac 1 {2g^2} \sigma^2 = -\frac 1 {2g^2}\left(\sigma^2 + 2g^2 \sigma \psibar_i \psi_i + g^4 (\psibar_i \psi_i)^2 - g^4 (\psibar_i \psi_i)^2\right)\\
&= \frac 12 g^2 (\psibar_i \psi_i)^2 - \frac 1 {2g^2}\left(\sigma + g^2 (\psibar_i \psi_i)\right)^2 
\end{align*}
Reinserting yields:
\begin{align*}
e^{i \int d^2x \frac 12 g^2 (\psibar_i \psi_i)^2} \underbrace{\int \mathcal D \sigma e^{-i \int d^2x \frac 1 {2g^2}\left(\sigma + g^2 (\psibar_i \psi_i)\right)^2}}_{= \int \mathcal D \sigma \exp(\frac {-i} {2g^2} \int d^2x \sigma^2) = \cst,\ \text{since it's gaussian}} \equiv e^{i \int d^2x \frac 12 g^2 (\psibar_i \psi_i)^2}
\end{align*}
\begin{align*}
\Rightarrow \int \mathcal D \psibar \mathcal D \psi \mathcal D \sigma e^{i \int d^2x \mathcal L_\sigma} \equiv \int \mathcal D \psibar \mathcal D \psi e^{i \int d^2x \left(\psibar_i \i \delslash \psi_i + \frac 12 g^2 (\psibar_i \psi_i)^2\right) } = \int \mathcal D \psibar \mathcal D \psi e^{i \int d^2x \mathcal L }
\end{align*}
\QED
\end{addmargin}

\textbf{Exercise 1c) -- Solution:}\\\
\begin{addmargin}[1cm]{0cm}
In order to calculate the leading order effective Potential $V_{eff}(\sigma)$, we split $\sigma$ in the classical part $\sigma_{cl}$ and $\hat \sigma$:
\begin{align*}
\sigma_{cl} \coloneqq \expval \sigma \ \ \ \ \  \hat \sigma \coloneqq \sigma - \sigma_{cl}\ \ \ \ \ \Rightarrow \sigma = \sigma_{cl} + \hat\sigma
\end{align*}
Next, we rewrite the path-integral, such that the action contains a gaussian integral in $\hat \sigma$ and a gaussian integral independent of $\hat \sigma$. For this we use, that $\sigma_{cl} = \cst$\footnote{Vacuum $\ket \Omega$ is invariant under Poincare-transformation: $x^\mu \to {\Lambda^\mu}_\nu x^\nu + a^\mu = x'^\mu$. Therefore: $\expval \sigma(x) = \underbrace{\bra \Omega U^{-1}}_{\bra \Omega} U \sigma U^{-1} \underbrace{U \ket \Omega}_{\ket \Omega} = \bra \Omega \sigma_{(\Lambda, a)} \ket \Omega = \expval \sigma(x')$ for all $(\Lambda, a)$. It follows that $\expval \sigma$ is constant.} and therefore $\mathcal D \sigma = \mathcal D \hat \sigma$
\begin{align*}
\int \mathcal D \psibar \mathcal D \psi \mathcal D &\sigma e^{i \int d^2x \mathcal L_\sigma} = \int \mathcal D \psibar \mathcal D \psi \mathcal D \hat \sigma e^{i \int d^2x \left[\psibar_i\left(\i \delslash - \sigma_{cl}\right)\psi_i - \frac 1 {2g^2}\left(\hat\sigma^2 + 2\sigma_{cl}\hat \sigma + 2g^2\hat \sigma \psibar_i\psi_i + \sigma_{cl}^2\right)\right] }\\
&\overset!= e^{\frac\i\hbar \int d^2x \left(\frac 12\del_\mu\sigma_{cl} \del^\mu \sigma_{cl} - V_{eff}^{(0)} - V_{eff}^{(1)} + o(\hbar^2)\  (+ \cst)\right)}
\end{align*}
In the last step we reintroduced $\hbar$ for formal reasons (see \textit{Steepest Descent}). We calculate $V_{eff}$ with the methods introduced by Jackiw (see also \url{https://doi.org/10.1103/PhysRevD.10.3235}). And we get:
\begin{flalign*}
\Rightarrow V_{eff}^{(0)}(\sigma_{cl}) =& \mathcal L_\sigma |_{\psi = \psibar = \hat \sigma = 0}  = \frac 1{2g^2}\sigma_{cl}^2&&
\end{flalign*}
\begin{flalign*}
\Rightarrow V_{eff}^{(1)}(\sigma_{cl}) =& \i \hbar \ln\left[ \det(\delta_{\hat \sigma}^2 \mathcal L_{\sigma})^{-1/2} \det(\delta_{\psibar} \delta_{\psi} \mathcal L_{\sigma}) \right] |_{\psi = \psibar = \hat \sigma = 0} \\
\overset{\hbar=1}=& \i \ln \det(\i\delslash - \sigma_{cl})^N\ (+\cst)\\
=& \i N \int \frac{d^2k}{(2\pi)^2}\ln \det(\i\delslash - \sigma_{cl})\\
=& \i N \int \frac{d^2k}{(2\pi)^2} \ln \det \begin{pmatrix} -\sigma_{cl} & -\i(k^0 + k^1) \\ \i(k^0 - k^1) & - \sigma_{cl} \end{pmatrix} \\
=& \i N \int \frac{d^2k}{(2\pi)^2} \ln(\sigma_{cl}^2 - k^2)\\
\overset{\text{Wick-rot.}}=& - N \int \frac{d^2k}{(2\pi)^2} \ln(\sigma_{cl}^2 + k^2) && (k^0, k^1) \to (+\i k^0, k^1)\\
\overset{\text{cut at $\Lambda$}}=& -N \int_0^\Lambda dk \frac k {2\pi} \ln(\sigma_{cl}^2 + k^2) \\
\equiv& -N \int_0^\Lambda dk \frac k {2\pi} \ln(1 + \left(\frac{\sigma_{cl}}k\right)^2)\\
\overset{PI}=& -\frac N{4\pi} \left[ \Lambda^2 \ln(1 + \left(\frac{\sigma_{cl}}\Lambda\right)^2) + \sigma_{cl}^2 \ln(1 + \left(\frac\Lambda{\sigma_{cl}}\right)^2) \right]\\
=& -\frac N{4\pi} \left[ \Lambda^2 \left( \left(\frac{\sigma_{cl}}\Lambda\right)^2 - \frac 12 \left(\frac{\sigma_{cl}}\Lambda\right)^4 + \dots \right) + \sigma_{cl}^2 \left(\ln(\left(\frac\Lambda{\sigma_{cl}}\right)^2) + \dots \right) \right]\\
=& -\frac N{4\pi} \sigma_{cl}^2 \left[ 1 + \ln( \left( \frac\Lambda{\sigma_{cl}} \right)^2 ) \right] + o(\Lambda^{-2})
\end{flalign*}
%Drop the hat in $\hat \sigma$:
\begin{align*}
\Rightarrow V_{eff} =  V_{eff}^{(0)} +  V_{eff}^{(1)} = \frac 1 {2g^2}\sigma_{cl}^2 -\frac N{4\pi} \sigma_{cl}^2 \left[ 1 + \ln( \left( \frac\Lambda{\sigma_{cl}} \right)^2 ) \right] = N \cdot f(N g^2)
\end{align*}
with
\begin{align*}
f(x) = \frac 1{2x}\sigma_{cl}^2-\frac 1{4\pi} \sigma_{cl}^2 \left[ 1 + \ln( \left( \frac\Lambda{\sigma_{cl}} \right)^2 ) \right]
\end{align*}
\end{addmargin}

\textbf{Exercise 1d) -- Solution:}\\\
\begin{addmargin}[1cm]{0cm}
We set the scale by introducing $\sigma_0$ and imposing: $1 \overset != \del_{\sigma_{cl}}^2 V_{eff} |_{\sigma_0}$
\begin{align*}
\Rightarrow& 1= g^{-2} - \frac N {2\pi}\left( 1 + \ln( \left( \frac\Lambda{\sigma_0} \right)^2 ) \right) + \frac {2N}\pi - \frac N {2\pi}\\
\Rightarrow& \left( 1 + \ln(\Lambda^2) \right) = \ln(\sigma_0^2) + 3 + \frac{2\pi}N(g^{-2} - 1)\\
\Rightarrow& V_{eff} = \frac 1{2g^2}\sigma_{cl}^2 - \frac N{4\pi} \sigma_{cl}^2 \left( 3 + \ln(\left(\frac{\sigma_0}{\sigma_{cl}}\right)^2)  + \frac{2\pi}N(g^{-2} - 1) \right)\\
&\ \ \ \ \ \  = - \frac N{4\pi} \sigma_{cl}^2 \left( 3 + \ln(\left(\frac{\sigma_0}{\sigma_{cl}}\right)^2)  - \frac{2\pi}N \right)
\end{align*}
\textit{(For some reason Bolun has here an additional factor of $2$)}.\\
We now minimize the potential:
\begin{align*}
0 \overset!=& \del_{\sigma_{cl}} V_{eff}\\ 
=& - \frac N{2\pi} \sigma_{cl} \left( 3 + \ln(\left(\frac{\sigma_0}{\sigma_{cl}}\right)^2)  - \frac{2\pi}N \right) + \frac N{2\pi} \sigma_{cl}\\
=& \frac N{2\pi} \sigma_{cl} \left( 2 + \ln(\left(\frac{\sigma_0}{\sigma_{cl}}\right)^2)  - \frac{2\pi}N \right)
\end{align*}
\begin{align*}
\Rightarrow& 0 \overset!=  2 + \ln(\left(\frac{\sigma_0}{\sigma_{cl}}\right)^2)  - \frac{2\pi}N\\
\Rightarrow& \sigma_{cl,\ min} = \pm \sigma_0 e^{1-\pi / N}\\
\Rightarrow& V_{eff,\ min} = -\frac N {4\pi} \sigma_0^2 e^{2-2\pi/N}
\end{align*}
We see that $\sigma_{cl,min}= \pm \sigma_0 e^{1-\pi / N} \neq 0$ is a solution for $\del_{\sigma_{cl}} V_{eff} \overset!=0$. Since $\del_{\sigma_{cl}}^2 V_{eff}|_{\sigma_{cl,min}} = 1 + \frac N{2\pi}\ln(\left(\frac{\sigma_{cl,min}}{\sigma_0}\right)^2) = \frac N\pi \overset !> 0$ is fulfilled, the solution is indeed a symmetry breaking groundstate. \QED
\end{addmargin}
\noindent\rule{\textwidth}{1pt}
\end{document}

