\documentclass[10pt]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage[a4paper,width=150mm,top=25mm,bottom=25mm]{geometry}
\usepackage{enumerate} %Roman enumeration
\usepackage{dsfont}
\usepackage{sectsty}
\usepackage{mathtools}
\usepackage{float}
\usepackage{physics}
\usepackage{contour}
\usepackage{pdfpages}
\renewcommand*{\thepage}{\large\arabic{page}}
\usepackage{mathrsfs}
\usepackage{adjustbox}
\usepackage{tabularx}


\usepackage{tikz}
\usetikzlibrary{graphdrawing}
\usepackage{luacode}
\begin{luacode}
  function pgf_lookup_and_require(name)
    local sep = '/'
    if string.find(os.getenv('PATH'),';') then
      sep = '\string\\'
    end
    local function lookup(name)
      local sub = name:gsub('%.',sep)
      local find_func = function (name, suffix)
        if resolvers then
          local n = resolvers.findfile (name.."."..suffix, suffix) -- changed
          return (not (n == '')) and n or nil
        else
          return kpse.find_file(name,suffix)
        end
      end
      if find_func(sub, 'lua') then
        require(name)
      elseif find_func(sub, 'clua') then
        collectgarbage('stop')
        require(name)
        collectgarbage('restart')
      else
        return false
      end
      return true
    end
    return
      lookup('pgf.gd.' .. name .. '.library') or
      lookup('pgf.gd.' .. name) or
      lookup(name .. '.library') or
      lookup(name)
  end
\end{luacode}
\usepackage{tikz-feynman}
\makeatletter
\tikzfeynmanset{compat=\tikzfeynman@version@major.\tikzfeynman@version@minor.\tikzfeynman@version@patch}
\makeatother
\allsectionsfont{\centering}

% New commands
\newcommand*{\QED}{\hfill\ensuremath{\blacksquare}}%
\newcommand*{\QEDB}{\hfill\ensuremath{\square}}%
\renewcommand*{\i}{\mathrm{i}}%
\newcommand*{\allimplies}{\big\} \hspace*{-0.2cm} \implies }%
\newcommand\hteq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny !}}}{=}}}
\newcommand\oneeq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny (i)}}}{=}}}
\newcommand\indeq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny ind}}}{=}}}
\newcommand\stareq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny \ref{eq-Lemma_1}}}}{=}}}


\begin{document}
% Original sheet
\includepdf[pages=-]{sheet5.pdf}
% Exercise 1
\noindent\rule{\textwidth}{2pt}
\textbf{Exercise 1 -- Solution:}\\\
\begin{addmargin}[1cm]{0cm}
\textit{Ad (a):} Let $A$ be the ordinary field, $Q$ an arbitrary Quantum field and by $B$ we denote the background field. The Lagrangian for $A$ and $Q$ is given by:
\begin{align*}
\mathscr{L}[A,Q, c,\bar{c}] &= \mathscr{L}_{G} + \mathscr{L}_{GF} + \mathscr{L}_{FP} \\ 
\mathscr{L}_{G} [A] &= - \frac{1}{4} \big( F^{a}_{\mu \nu} \big)^{2} \\
\mathscr{L}_{GF} [Q] &= -\frac{1}{2\xi} \big( G^{a}(Q) \big)^{2}, \qquad G^{a}(Q) = (D_{\mu}(B) Q^{\mu} )^{a}\\
\mathscr{L}_{FP}[A,Q,c,\bar{c}] &= - \bar{c}^{a} \big(D^{\mu}(B) D_{\mu}(Q+B) \big)^{ab} c^{b}
\end{align*}
We can derive the Feynman rules in the background field gauge by simply inserting $A = Q + B$. We get the following for the kinetical term:
\begin{align*}
\mathscr{L}_{G}[Q+B] = &- \frac{1}{4} \big( \partial_{\mu} (Q+B)^{a}_{\nu} - \partial_{\nu} (Q+B)_{\mu}^{a} + g f^{abc} (Q+B)^{b}_{\mu} (Q+B)^{c}_{\nu} \big)^{2} \\
= &\frac{1}{2} (Q+B)^{a}_{\mu} \big( g^{\mu \nu} \Box - \partial^{\mu} \partial^{\nu} \big) (Q+B)^{a}_{\nu} 
- \frac{1}{2} g f^{abc} (\partial_{\mu} Q^{a}_{\nu}) Q^{b,\mu} Q^{c,\nu} \\
&- \frac{1}{2} g f^{abc} (\partial_{\mu} B^{a}_{\nu}) B^{b,\mu} B^{c,\nu} 
- \frac{1}{4} g^{2} f^{abc} f^{ade} Q^{b,\mu} Q^{c,\nu} Q^{d}_{\mu} Q^{e}_{\nu} \\
&- \frac{1}{4} g^{2} f^{abc} f^{ade} B^{b,\mu} B^{c,\nu} B^{d}_{\mu} B^{e}_{\nu} + \text{other}
\end{align*}
Here we denote all other terms like $BBQQ$ and more by 'others'. For the Gauge Fixing term we get:
\begin{align*}
\mathscr{L}_{GF} = - \frac{1}{2\xi} \big( \partial_{\mu} Q^{a, \mu} \big)^{2} -\frac{1}{\xi} g f^{acb} B^{c}_{\mu} Q^{b,\mu} \partial_{\nu} Q^{a,\mu} - \frac{1}{2\xi} g^{2} f^{acb}f^{aed} B^{c}_{\mu} Q^{b, \mu} B^{e}_{\nu} Q^{d, \nu}
\end{align*}
The Faddeev-Popov term is given by:
\begin{align*}
\mathscr{L}_{FP} = &- \bar{c}^{a} \Box c^{a} + g f^{aeb} (\partial^{\mu} \bar{c}^{a}) Q^{e}_{\mu} c^{b} + g f^{aeb} (\partial^{\mu} \bar{c}^{a}) B^{e}_{\mu} c^{b} - g f^{adb} \bar{c}^{a} B^{d,\mu} (\partial_{\mu} c^{b}) \\ 
&- g^{2} f^{adc} f^{ceb} \bar{c}^{a} B^{d,\mu} Q^{e}_{\mu} c^{b} - g^{2} f^{adc} f^{ceb} \bar{c}^{a} B^{d,\mu} B^{e}_{\mu} c^{b}
\end{align*}
The first thing that we see is that the quadratic terms do not change. We therefore obtain our old propagtors for the fields and the ghosts:
\begin{table}[H]
\centering
\begin{tabular}{cl}
% Fermion Propagator
\feynmandiagram [inline=(a.base), horizontal=a to b] {
a [particle={\(a, \mu\)}] -- [momentum=$k$] b [particle={\( b, \nu\)}] }; 
& 
$
= \dfrac{- \i \delta_{ab}}{k^{2}+\i \epsilon} \bigg[ g_{\mu \nu} - \dfrac{k_{\mu} k_{\nu}}{k^{2}} ( 1 - \xi ) \bigg] 
$ \\[0.5cm]
%Ghost propagator
\feynmandiagram [inline=(a.base), horizontal=a to b] {
a [particle={\(a\)}] -- [ghost, momentum=$k$] b [particle={\( b\)}] }; 
&
$
= \dfrac{\i \delta_{ab}}{k^{2}+\i \epsilon}
$
\end{tabular}
\end{table}
The next thing to notice is that only $BQQ$ and $BQBQ$ vertices get contributions from the Gauge Fixing part. We therefore obtain for $BQQ$ from 'Gauge-Fixing' and the kinetical part:
\begin{table}[H]
\centering
\begin{tabular}{cl}
%FQQ
\feynmandiagram [inline=(d.base), horizontal=d to b] {
a [particle={\(b, \nu \)}] -- [gluon, edge label=$q$] b [dot] -- [gluon, edge label=$r$] c [particle={\( c, \lambda \)}],
b -- [edge label=$p$] d [particle={\(a, \mu\)}], };
&
$
=  g f^{abc} \big[ g_{\mu \lambda} \big(p-r-\frac{1}{\xi} q \big)_{\nu} +g_{\nu \lambda} (r-q)_{\mu} + g_{\mu \nu} \big( q-p+\frac{1}{\xi} r \big)_{\lambda}  \big] 
$
\end{tabular}
\end{table}
Further we get the following for $BQBQ$
\begin{table}[H]
\centering
\begin{tabularx}{0.7\textwidth}{cX}
%FFQQ
\vspace{0pt}
\begin{tikzpicture}[baseline]
    \begin{feynman}
   \vertex[blob] (m) at ( 0, 0) ;
   \draw[fill=black] (0,0) circle[radius=0.075]; 
    \vertex (a) at (-1,-1) {$b, \nu$};
    \vertex (b) at ( 1,-1) {$c, \lambda$};
    \vertex (c) at (-1, 1) {$a, \mu$};
    \vertex (d) at ( 1, 1) {$d, \rho$};
    \diagram* {
      (d) -- [] (m) -- [gluon] (c),
      (b) -- [gluon] (m) -- [] (a),
    };
  \end{feynman}
\end{tikzpicture}
&
$
= \left\{
\begin{matrix}
 - \i g^{2}   \big[f^{abx} f^{xcd} \big(g_{\mu \lambda} g_{\nu \rho} - g_{\mu \rho} g_{\nu \lambda} + \frac{1}{\xi} g_{\mu \nu} g_{\lambda \rho} \big) \\
\qquad \quad+ f^{adx} f^{xbc} \big(g_{\mu \nu} g_{\lambda \rho} - g_{\mu \lambda} g_{\nu\rho } +\frac{1}{\xi} g_{\mu \rho} g_{\nu \lambda} \big) \\
+ f^{acx} f^{xbd} (g_{\mu \nu} g_{\lambda \rho} - g_{\mu \rho} g_{\nu \lambda})   \big]  \\
\end{matrix} \right.
$ 
\end{tabularx}
\end{table}
From here we notice that vertices like $QQQ, BQQQ$ and $QQQQ$ simply emerge from the 3- and 4-vertex from above by eliminating the gauge fixing part, i.e. $\xi \uparrow \infty$.
\begin{table}[H]
\centering
\begin{tabularx}{0.8\textwidth}{cX}
%QQQ
\feynmandiagram [inline=(d.base), horizontal=d to b] {
a [particle={\(b, \nu \)}] -- [gluon, edge label=$q$] b [dot] -- [gluon, edge label=$r$] c [particle={\( c, \lambda \)}],
b -- [gluon, edge label=$p$] d [particle={\(a, \mu\)}], };
&
$
=  g f^{abc} \big[ g_{\mu \lambda} (p-r)_{\nu} +g_{\nu \lambda} (r-q)_{\mu} + g_{\mu \nu} ( q-p)_{\lambda}  \big] 
$ \\
%QQQQ 
\vspace{0pt}
\begin{tikzpicture}[baseline]
    \begin{feynman}
   \vertex[blob] (m) at ( 0, 0) ;
   \draw[fill=black] (0,0) circle[radius=0.075]; 
    \vertex (a) at (-1,-1) {$b, \nu$};
    \vertex (b) at ( 1,-1) {$c, \lambda$};
    \vertex (c) at (-1, 1) {$a, \mu$};
    \vertex (d) at ( 1, 1) {$d, \rho$};
    \diagram* {
      (d) -- [gluon] (m) -- [gluon] (c),
      (b) -- [gluon] (m) -- [gluon] (a),
    };
  \end{feynman}
\end{tikzpicture}
&
$
= \left\{
\begin{matrix}
 - \i g^{2}   \big[f^{abx} f^{xcd} (g_{\mu \lambda} g_{\nu \rho} - g_{\mu \rho} g_{\nu \lambda})  \\
\qquad \quad + f^{adx} f^{xbc} (g_{\mu \nu} g_{\lambda \rho} - g_{\mu \lambda} g_{\nu\rho }) \\
\qquad \qquad + f^{acx} f^{xbd} (g_{\mu \nu} g_{\lambda \rho} - g_{\mu \rho} g_{\nu \lambda})   \big]  
\end{matrix} \right.
$ \\
%FQQQ
\vspace{0pt}
\begin{tikzpicture}[baseline]
    \begin{feynman}
   \vertex[blob] (m) at ( 0, 0) ;
   \draw[fill=black] (0,0) circle[radius=0.075]; 
    \vertex (a) at (-1,-1) {$b, \nu$};
    \vertex (b) at ( 1,-1) {$c, \lambda$};
    \vertex (c) at (-1, 1) {$a, \mu$};
    \vertex (d) at ( 1, 1) {$d, \rho$};
    \diagram* {
      (d) -- [] (m) -- [gluon] (c),
      (b) -- [gluon] (m) -- [gluon] (a),
    };
  \end{feynman}
\end{tikzpicture}
&
$
= \left\{
\begin{matrix}
 - \i g^{2}   \big[f^{abx} f^{xcd} (g_{\mu \lambda} g_{\nu \rho} - g_{\mu \rho} g_{\nu \lambda})  \\
\qquad \quad + f^{adx} f^{xbc} (g_{\mu \nu} g_{\lambda \rho} - g_{\mu \lambda} g_{\nu\rho }) \\
\qquad \qquad + f^{acx} f^{xbd} (g_{\mu \nu} g_{\lambda \rho} - g_{\mu \rho} g_{\nu \lambda})   \big]  
\end{matrix} \right.
$ 
\end{tabularx}
\end{table}
Lastly we have to consider vertices concerning ghosts. We can immediately read everything of from the Faddeev-Popov Lagrangian. 
\begin{table}[H]
\centering
\begin{tabularx}{0.7\textwidth}{cX}
%GGF
\feynmandiagram [inline=(d.base), vertical=a to c] {
a [particle={\(a\)}] -- [ghost, edge label=$p$] b [dot] -- [ghost, edge label=$q$] c [particle={\( b\)}],
b -- [anti fermion] d [particle={\(c, \mu\)}], };
&
$
=  -g f^{acb} (p+q)_{\mu}
$ \\
%GGQ
\feynmandiagram [inline=(d.base), vertical=a to c] {
a [particle={\(a\)}] -- [ghost, edge label=$p$] b [dot] -- [ghost, edge label=$q$] c [particle={\( b\)}],
b -- [gluon] d [particle={\(c, \mu\)}], };
&
$
= -g f^{acb} p_{\mu}
$ \\
%GGQF
\vspace{0pt}
\begin{tikzpicture}[baseline]
    \begin{feynman}
   \vertex[blob] (m) at ( 0, 0) ;
   \draw[fill=black] (0,0) circle[radius=0.075]; 
    \vertex (a) at (-1,-1) {$c, \mu$};
    \vertex (b) at ( 1,-1) {$d, \nu$};
    \vertex (c) at (-1, 1) {$a$};
    \vertex (d) at ( 1, 1) {$b$};
    \diagram* {
      (d) -- [ghost] (m) -- [ghost] (c),
      (b) -- [gluon] (m) -- [] (a),
    };
  \end{feynman}
\end{tikzpicture}
&
$
= - \i g^{2} f^{acx} f^{xdb} g_{\mu \nu}
$ \\
%FFGG
\vspace{0pt}
\begin{tikzpicture}[baseline]
    \begin{feynman}
   \vertex[blob] (m) at ( 0, 0) ;
   \draw[fill=black] (0,0) circle[radius=0.075]; 
    \vertex (a) at (-1,-1) {$c, \mu$};
    \vertex (b) at ( 1,-1) {$d, \nu$};
    \vertex (c) at (-1, 1) {$a$};
    \vertex (d) at ( 1, 1) {$b$};
    \diagram* {
      (d) -- [ghost] (m) -- [ghost] (c),
      (b) -- [] (m) -- [] (a),
    };
  \end{feynman}
\end{tikzpicture}
&
$
= - \i g^{2} \, g_{\mu \nu} (f^{acx} f^{xdb} + f^{adx} f^{xcb})
$ 
\end{tabularx}
\end{table}
\vspace*{-0.4cm}
This closes our derivation of the Feynman Rules for the Background Field gauge. On the next page all Feynman Rules are stated as one.





\newpage
\vspace*{-2cm}
\begin{table}[H]
\centering
\caption{Feynman rules for background field gauge (\textit{Introduction to the Background Field Method}, \textsc{L.F. Abbott (1981)})} 
\begin{tabularx}{0.8\textwidth}{cX}
% Fermion Propagator
\feynmandiagram [inline=(a.base), horizontal=a to b] {
a [particle={\(a, \mu\)}] -- [fermion, momentum=$k$] b [particle={\( b, \nu\)}] }; 
& 
$
= \dfrac{-\i \delta_{ab}}{k^{2}+\i \epsilon} \big[ g_{\mu \nu} - \frac{k_{\mu} k_{\nu}}{k^{2}} ( 1 - \xi ) \big] 
$ \\[0.5cm]
%Ghost propagator
\feynmandiagram [inline=(a.base), horizontal=a to b] {
a [particle={\(a\)}] -- [ghost, momentum=$k$] b [particle={\( b\)}] }; 
&
$
= \dfrac{\i \delta_{ab}}{k^{2}+\i \epsilon}
$ \\
%FQQ
\feynmandiagram [inline=(d.base), horizontal=d to b] {
a [particle={\(b, \nu \)}] -- [gluon, edge label=$q$] b [dot] -- [gluon,edge label=$r$] c [particle={\( c, \lambda \)}],
b -- [edge label=$p$] d [particle={\(a, \mu\)}], };
&
$
=  g f^{abc} \big[ g_{\mu \lambda} \big(p-r-\frac{1}{\xi} q \big)_{\nu} +g_{\nu \lambda} (r-q)_{\mu} + g_{\mu \nu} \big( q-p+\frac{1}{\xi} r \big)_{\lambda}  \big] 
$ \\

%QQQ
\feynmandiagram [inline=(d.base), horizontal=d to b] {
a [particle={\(b, \nu \)}] -- [gluon, edge label=$q$] b [dot] -- [gluon, edge label=$p$] c [particle={\( c, \lambda \)}],
b -- [gluon, edge label=$p$] d [particle={\(a, \mu\)}], };
&
$
=  g f^{abc} \big[ g_{\mu \lambda} (p-r)_{\nu} +g_{\nu \lambda} (r-q)_{\mu} + g_{\mu \nu} ( q-p)_{\lambda}  \big] 
$
\\
%QQQQ 
\vspace{0pt}
\begin{tikzpicture}[baseline]
    \begin{feynman}
   \vertex[blob] (m) at ( 0, 0) ;
   \draw[fill=black] (0,0) circle[radius=0.075]; 
    \vertex (a) at (-1,-1) {$b, \nu$};
    \vertex (b) at ( 1,-1) {$c, \lambda$};
    \vertex (c) at (-1, 1) {$a, \mu$};
    \vertex (d) at ( 1, 1) {$d, \rho$};
    \diagram* {
      (d) -- [gluon] (m) -- [gluon] (c),
      (b) -- [gluon] (m) -- [gluon] (a),
    };
  \end{feynman}
\end{tikzpicture}
&
$
= \left\{
\begin{matrix}
 - \i g^{2}   \big[f^{abx} f^{xcd} (g_{\mu \lambda} g_{\nu \rho} - g_{\mu \rho} g_{\nu \lambda})  \\
\qquad \quad + f^{adx} f^{xbc} (g_{\mu \nu} g_{\lambda \rho} - g_{\mu \lambda} g_{\nu\rho }) \\
\qquad \qquad + f^{acx} f^{xbd} (g_{\mu \nu} g_{\lambda \rho} - g_{\mu \rho} g_{\nu \lambda})   \big]  
\end{matrix} \right.
$ \\
%FQQQ
\vspace{0pt}
\begin{tikzpicture}[baseline]
    \begin{feynman}
   \vertex[blob] (m) at ( 0, 0) ;
   \draw[fill=black] (0,0) circle[radius=0.075]; 
    \vertex (a) at (-1,-1) {$b, \nu$};
    \vertex (b) at ( 1,-1) {$c, \lambda$};
    \vertex (c) at (-1, 1) {$a, \mu$};
    \vertex (d) at ( 1, 1) {$d, \rho$};
    \diagram* {
      (d) -- [] (m) -- [gluon] (c),
      (b) -- [gluon] (m) -- [gluon] (a),
    };
  \end{feynman}
\end{tikzpicture}
&
$
= \left\{
\begin{matrix}
 - \i g^{2}   \big[f^{abx} f^{xcd} (g_{\mu \lambda} g_{\nu \rho} - g_{\mu \rho} g_{\nu \lambda})  \\
\qquad \quad + f^{adx} f^{xbc} (g_{\mu \nu} g_{\lambda \rho} - g_{\mu \lambda} g_{\nu\rho }) \\
\qquad \qquad + f^{acx} f^{xbd} (g_{\mu \nu} g_{\lambda \rho} - g_{\mu \rho} g_{\nu \lambda})   \big]  
\end{matrix} \right.
$ \\
%FFQQ
\vspace{0pt}
\begin{tikzpicture}[baseline]
    \begin{feynman}
   \vertex[blob] (m) at ( 0, 0) ;
   \draw[fill=black] (0,0) circle[radius=0.075]; 
    \vertex (a) at (-1,-1) {$b, \nu$};
    \vertex (b) at ( 1,-1) {$c, \lambda$};
    \vertex (c) at (-1, 1) {$a, \mu$};
    \vertex (d) at ( 1, 1) {$d, \rho$};
    \diagram* {
      (d) -- [] (m) -- [gluon] (c),
      (b) -- [gluon] (m) -- [] (a),
    };
  \end{feynman}
\end{tikzpicture}
&
$
= \left\{
\begin{matrix}
 - \i g^{2}   \big[f^{abx} f^{xcd} \big(g_{\mu \lambda} g_{\nu \rho} - g_{\mu \rho} g_{\nu \lambda} + \frac{1}{\xi} g_{\mu \nu} g_{\lambda \rho} \big) \\
\qquad \quad+ f^{adx} f^{xbc} \big(g_{\mu \nu} g_{\lambda \rho} - g_{\mu \lambda} g_{\nu\rho } +\frac{1}{\xi} g_{\mu \rho} g_{\nu \lambda} \big) \\
+ f^{acx} f^{xbd} (g_{\mu \nu} g_{\lambda \rho} - g_{\mu \rho} g_{\nu \lambda})   \big]  \\
\end{matrix} \right.
$ \\
%GGF
\feynmandiagram [inline=(d.base), vertical=a to c] {
a [particle={\(a\)}] -- [ghost, edge label=$p$] b [dot] -- [ghost,edge label=$q$] c [particle={\( b\)}],
b -- [] d [particle={\(c, \mu\)}], };
&
$
=  -g f^{acb} (p+q)_{\mu}
$ \\
%GGQ
\feynmandiagram [inline=(d.base), vertical=a to c] {
a [particle={\(a\)}] -- [ghost, edge label=$p$] b [dot] -- [ghost, edge label=$q$] c [particle={\( b\)}],
b -- [gluon] d [particle={\(c, \mu\)}], };
&
$
= -g f^{acb} p_{\mu}
$ \\
%GGQF
\vspace{0pt}
\begin{tikzpicture}[baseline]
    \begin{feynman}
   \vertex[blob] (m) at ( 0, 0) ;
   \draw[fill=black] (0,0) circle[radius=0.075]; 
    \vertex (a) at (-1,-1) {$c, \mu$};
    \vertex (b) at ( 1,-1) {$d, \nu$};
    \vertex (c) at (-1, 1) {$a$};
    \vertex (d) at ( 1, 1) {$b$};
    \diagram* {
      (d) -- [ghost] (m) -- [ghost] (c),
      (b) -- [gluon] (m) -- [] (a),
    };
  \end{feynman}
\end{tikzpicture}
&
$
= - \i g^{2} f^{acx} f^{xdb} g_{\mu \nu}
$ \\
%FFGG
\vspace{0pt}
\begin{tikzpicture}[baseline]
    \begin{feynman}
   \vertex[blob] (m) at ( 0, 0) ;
   \draw[fill=black] (0,0) circle[radius=0.075]; 
    \vertex (a) at (-1,-1) {$c, \mu$};
    \vertex (b) at ( 1,-1) {$d, \nu$};
    \vertex (c) at (-1, 1) {$a$};
    \vertex (d) at ( 1, 1) {$b$};
    \diagram* {
      (d) -- [ghost] (m) -- [ghost] (c),
      (b) -- [] (m) -- [] (a),
    };
  \end{feynman}
\end{tikzpicture}
&
$
= - \i g^{2} \, g_{\mu \nu} (f^{acx} f^{xdb} + f^{adx} f^{xcb})
$ 
\end{tabularx}
\end{table}
\newpage

\textit{Ad (b):} % 1-loop graphs
\begin{figure}[H]
\centering
  \begin{tabular}{@{}cc@{}}
   %1st Graph
\feynmandiagram [layered layout, horizontal=b to c] { 
a [particle=\(B\)] -- [] b [dot]
-- [ghost, half left, looseness=1.7, edge label=] c [dot]
-- [ghost, half left, looseness=1.7, edge label=] b,
c -- [] d [particle=\(B\)],
}; &
   %2nd Graph
   \feynmandiagram [layered layout, horizontal=b to c] { 
a [particle=\(B\)] -- [] b [dot]
-- [gluon, half left, looseness=1.7, edge label=\(Q\)] c [dot]
-- [gluon, half left, looseness=1.7, edge label=\(Q\)] b,
c -- [] d [particle=\(B\)],
}; \\
   %3rd Graph
      \begin{tikzpicture}
\begin{feynman}
\diagram [horizontal=a to b, layered layout] {
  a [particle=\(B\)] -- [] b [dot]--b-- [] c [particle=\(B\)]
};
\path (b)--++(90:0.6) coordinate (A);
    \draw (b) [ghost] arc [start angle=270, end angle=-90, radius=0.7cm] node at (1.5,1.7) {};
\end{feynman}
\end{tikzpicture} &
   %4th Graph
\begin{tikzpicture}
\begin{feynman}
\diagram [horizontal=a to b, layered layout] {
  a [particle=\(B\)] -- [] b [dot]--b-- [] c [particle=\(B\)]
};
\path (b) --++(90:0.6) coordinate (A);
    \draw (b) [gluon] arc [start angle=270, end angle=-90, radius=0.7cm] node at (1.5,1.7) {$Q$};
\end{feynman}
\end{tikzpicture} \\
  \end{tabular}
  \caption{1-loop diagrams for Feynman rules in Background field gauge}
\end{figure}
I.e the $B$-field is only an external field.
\end{addmargin}
\noindent\rule{\textwidth}{1pt}
\end{document}

