\documentclass[10pt]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage[a4paper,width=150mm,top=25mm,bottom=25mm]{geometry}
\usepackage{enumerate} %Roman enumeration
\usepackage{dsfont}
\usepackage{sectsty}
\usepackage{mathtools}
\usepackage{float}
\usepackage{physics}
\usepackage{pdfpages}
\renewcommand*{\thepage}{\large\arabic{page}}

\allsectionsfont{\centering}

% New commands
\newcommand*{\QED}{\hfill\ensuremath{\blacksquare}}%
\newcommand*{\QEDB}{\hfill\ensuremath{\square}}%
\newcommand*{\sg}{\boldsymbol{\sigma}}%
\newcommand*{\e}{\mathbf{e}}%
\renewcommand*{\i}{\mathrm{i}}%
\newcommand*{\allimplies}{\big\} \hspace*{-0.2cm} \implies }%
\newcommand\hteq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny !}}}{=}}}
\newcommand\oneeq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny (i)}}}{=}}}
\newcommand\indeq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny ind}}}{=}}}
\newcommand\stareq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny \ref{eq-Lemma_1}}}}{=}}}


\begin{document}
% Original sheet
\includepdf[pages=-]{sheet6.pdf}
% Exercise 1
\noindent\rule{\textwidth}{2pt}
\textbf{Exercise 1 -- Solution:}\\\
\begin{addmargin}[1cm]{0cm}
First we want to show equation of motion can be rewritten in the form given above. We start by calculation the cross product:
\begin{align*}\label{eq-cross}
&\:\sg_{n} \cross \sg_{n+1} = \varepsilon^{ijk} \sigma_{n}^{i} \sigma_{n+1}^{j} \e^{j} \\
\Rightarrow &\: 2 \epsilon ( \sg_{n} \cross \sg_{n+1} - \sg_{n-1} \cross \sg_{n} ) = 2 \epsilon \, \varepsilon^{ijk} (\sigma_{n}^{i} \sigma_{n+1}^{j} - \sigma_{n-1}^{i} \sigma^{j}_{n}) \e^{k} \tag{$\ast$}
\end{align*}
Next we need to consider:
\begin{align*}
H\sg_{n} - \sg_{n} H &= - \epsilon \sum_{n^{\prime}=1}^{N} \big[   (  \sigma_{n^{\prime}}^{l} \sigma_{n^{\prime} + 1}^{l} \sigma^{k}_{n} - \sigma^{k}_{n} \sigma_{n^{\prime}}^{l} \sigma_{n^{\prime} + 1}^{l} )\e^{k}  \big] \\
&= - \epsilon \sum_{n^{\prime}=1}^{N} \big[   (  \sigma_{n^{\prime}}^{l} \sigma_{n^{\prime} + 1}^{l} \sigma^{k}_{n} -\sigma_{n^{\prime}}^{l} \sigma^{k}_{n}  \sigma_{n^{\prime} + 1}^{l}  - [\sigma^{k}_{n}, \sigma^{l}_{n^{\prime}}] \sigma^{l}_{n^{\prime}+1}) \e^{k} \big] = \ldots
\end{align*}
Now we must use the commutation relation for the Pauli matrices given as:
\begin{equation*}
[\sigma^{k}_{n}, \sigma^{l}_{n^{\prime}}] = 2 \i \delta_{n n^{\prime}} \varepsilon^{klm} \sigma^{m}_{n}
\end{equation*}
With this we can proceed with our calculation:
\begin{align*}
\ldots &= \epsilon \sum_{n^{\prime} =  1}^{N} \big[ ( 2 \i \delta_{n n^{\prime}} \varepsilon^{klm} \sigma^{m}_{n} \sigma^{l}_{n^{\prime} + 1} + \sigma^{l}_{n^{\prime}} [\sigma^{k}_{n}, \sigma^{l}_{n^{\prime}+1}]  \e^{k}) \big] \\
&= 2 \i \epsilon \, \varepsilon^{klm} (\sigma^{m}_{n} \sigma^{l}_{n+1} + \sigma^{l}_{n-1} \sigma^{m}_{n}) \e^{k} \\
&= - 2 \i \epsilon \, \varepsilon^{ijk} (\sigma^{i}_{n} \sigma^{j}_{n+1} - \sigma^{i}_{n-1} \sigma^{j}_{n}) \e^{k}
\end{align*}
Comparing this result to (\ref{eq-cross}), we conclude:
\begin{equation*}
\hbar \dot{\sg}_{n} = 2 \epsilon (\sg_{n} \cross \sg_{n+1} - \sg_{n-1} \cross \sg_{n})
\end{equation*}
\QED \\
\noindent\rule{0.925\textwidth}{0.5pt} \\
\textit{Own Solution (long):} The next step is to solve Eq. \textit{(4)}. We do this by first introducing the 'ladder'-operators $\sg_{n}^{\pm} := \sg_{n,x} \pm \i \sg_{n,y}$. This yields:
\begin{equation*}\label{eq-lad}
\dot{\sg}_{n}^{\pm} = \mp \i \frac{2 \epsilon}{\hbar} (2 \sg_{n}^{\pm} - \sg_{n+1}^{\pm} - \sg_{n-1}^{\pm} ) =: (\tilde{V}^{\pm} \sg^{\pm})_{n} \tag{$\ast \ast$}
\end{equation*}
The next step is to notice that our system is invariant under the symmetry
\begin{equation*}
\mathcal{S}: \sg_{n} \mapsto \sg_{n+1}
\end{equation*}
We need to solve the eigenvalue equation for $\mathcal{S}$:
\begin{equation*}
\mathcal{S}\sg = \lambda \sg \Rightarrow \sg_{l+1} = \lambda \sg_{l} = \lambda^{l} \sg_{1}
\end{equation*}
Since we are using periodic boundary conditions on $n$, i.e. $\sg_{N+1}=\sg_{1}$, we conclude
\begin{equation*}
\sg_{1} = \lambda^{N} \sg_{1}
\end{equation*}
This directly implies that our eigenvalues and eigenvectors are of the form:
\begin{align*}
\lambda &= e^{\i l \delta} = e^{\i l \frac{2\pi}{N}} \\
\e_{l} &\propto \big(1,e^{\i l \delta}, e^{2 \i \delta}, \ldots , e^{(N-1) \i l \delta} \big)^{\top}
\end{align*}
We can now easily solve (\ref{eq-lad}), since $[\tilde{V},\mathcal{S}] = 0$ and the eigenspaces of $\mathcal{S}$ are all one-dimensional:
\begin{equation*}
\tilde{V}^{\pm} \e^{\pm}_{l} = \xi^{\pm}_{l} \e^{\pm}_{l} = \mp \i \frac{2\epsilon}{\hbar} (2 - e^{\i l \delta} - e^{- \i l \delta}) \e_{l} = \mp \i \frac{8 \epsilon}{\hbar} \sin^{2} \bigg( \frac{l \delta}{2} \bigg) \e^{\pm}_{l} 
\end{equation*}
With this determining $\xi^{\pm}_{l}$ we can simply solve the differential equation by:
\begin{equation*}
\tilde{V}^{\pm} \e^{\pm}_{l} = \dot{\e}^{\pm}_{l} = \xi^{\pm}_{l} \e^{\pm}_{l} \Rightarrow \e^{\pm}_{l} = A^{\pm}_{l} e^{\xi^{\pm}_{l} t}
\end{equation*}
We can now go back from the 'ladder'-operators to $\sg_{n,x}$ and $\sg_{n,y}$:
\begin{align*}
\sg_{n,x} &= \frac{1}{2} \sum_{l} \big( A^{+}_{l} e^{\xi^{+}_{l} t} + A^{-}_{l} e^{\xi^{-}_{l} t} \big) \\
&= \sum_{l} \alpha_{l} \cos \bigg\{ \frac{8 \epsilon}{\hbar} \sin^{2} \bigg(\frac{l \delta}{2} \bigg) t + \phi_{l} \bigg\} \\ 
\sg_{n,y} &= \sum_{l} \beta_{l} \cos \bigg\{ \underbrace{\frac{8 \epsilon}{\hbar} \sin^{2} \bigg(\frac{l \delta}{2} \bigg)}_{\omega_{l}} t + \psi_{l} \bigg\}
\end{align*}
We can simply read of our frequency given as:
\begin{align*}
\hbar \omega_{l} &= 8\epsilon \sin^{2} \bigg( \frac{l \delta}{2} \bigg) \\
&= 8 \epsilon \sin^{2} \bigg( \frac{k_{l} a}{2} \bigg)
\end{align*}
Where in the end we defined the wavenumber $k_{l}:=\frac{l \delta}{a} = l \frac{2 \pi}{Na}$.
\QED \\
\noindent\rule{0.925\textwidth}{0.5pt} \\
\textit{Bolun's solution (short):} We can also solve the problem as follows. We approximate $\sigma_{z} \approx 1$. Then we can write:
\begin{equation*}
\sigma_{z} \sigma_{x} = \i \sigma_{y} =\sigma_{x}
\end{equation*} 
Next we make the ansatz:
\begin{equation*}
\sigma_{x} \propto e^{-\i (\omega t - kna)}, \qquad kNa = 2 \pi 
\end{equation*}
Inserting this into the equation we obtain:
\begin{align*}
\hbar ( - \i \omega ) \sigma_{x} = 2 \epsilon ( \underbrace{-2 \i  + \i e^{\i k a} + \i e^{-\i ka}  }_{- \i 4 \sin^{2} ( \frac{ka}{a} ) }) \sigma_{x}
\end{align*}
Solving for $\omega$ yields:
\begin{equation*}
\omega = 8 \epsilon \sin^{2} \bigg( \frac{ka}{2} \bigg) 
\end{equation*} \QED
\end{addmargin}
\noindent\rule{\textwidth}{1pt}
\end{document}

