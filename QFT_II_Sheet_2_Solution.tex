\documentclass[10pt]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb}
\usepackage{graphicx}
\usepackage[a4paper,width=150mm,top=25mm,bottom=25mm]{geometry}
\usepackage{enumerate} %Roman enumeration
\usepackage{dsfont}
\usepackage{sectsty}
\usepackage{mathtools}
\usepackage{float}
\usepackage{physics}
\usepackage{pdfpages}
\usepackage{systeme}
\renewcommand*{\thepage}{\large\arabic{page}}
\usepackage{ mathrsfs }

\allsectionsfont{\centering}

\usepackage{tikz}
\usetikzlibrary{graphdrawing}
\usepackage{luacode}
\begin{luacode}
	function pgf_lookup_and_require(name)
	local sep = '/'
	if string.find(os.getenv('PATH'),';') then
	sep = '\string\\'
	end
	local function lookup(name)
	local sub = name:gsub('%.',sep)
	local find_func = function (name, suffix)
	if resolvers then
	local n = resolvers.findfile (name.."."..suffix, suffix) -- changed
	return (not (n == '')) and n or nil
	else
	return kpse.find_file(name,suffix)
	end
	end
	if find_func(sub, 'lua') then
	require(name)
	elseif find_func(sub, 'clua') then
	collectgarbage('stop')
	require(name)
	collectgarbage('restart')
	else
	return false
	end
	return true
	end
	return
	lookup('pgf.gd.' .. name .. '.library') or
	lookup('pgf.gd.' .. name) or
	lookup(name .. '.library') or
	lookup(name)
	end
\end{luacode}
\usepackage{tikz-feynman}
%\tikzfeynmanset{compat=\tikzfeynman@version@major.\tikzfeynman@version@minor.\tikzfeynman@version@patch}

% New commands
\newcommand*{\QED}{\hfill\ensuremath{\blacksquare}}%
\newcommand*{\QEDB}{\hfill\ensuremath{\square}}%
\renewcommand*{\i}{\mathrm{i}}%
\newcommand*{\allimplies}{\big\} \hspace*{-0.2cm} \implies }%
\newcommand\hteq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny !}}}{=}}}
\newcommand\oneeq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny (i)}}}{=}}}
\newcommand\indeq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny ind}}}{=}}}
\newcommand\stareq{\mathrel{\stackrel{\makebox[0pt]{\mbox{\normalfont\tiny \ref{eq-Lemma_1}}}}{=}}}

\newcommand\del\partial
\renewcommand\comm[2]{\left[#1,\ #2\right]}
\renewcommand\acomm[2]{\left\{#1,\ #2\right\}}
\newcommand\D{\mathcal D}
\newcommand\circled[1]{\raisebox{.5pt}{\textcircled{\raisebox{-.9pt} {#1}}}}

\begin{document}
% Original sheet
\includepdf[pages=-]{sheet2.pdf}

%\iffalse
% Exercise 1
\noindent\rule{\textwidth}{2pt}
\textbf{Exercise 1 -- Solution:}\\\
\begin{addmargin}[1cm]{0cm}
To include a gauge constraint into our Path Integral we will use the method proposed by Faddeev and Popov. We introduce the Faddeev-Popov determinant by:
\begin{align*}
{\Delta_{FP}}^{-1}&\coloneqq\int \mathcal{D} \epsilon (x) \delta (G^{a}(A^{\epsilon})) \\
\delta (G^{a}(A^{\epsilon})) &= \delta (\epsilon) \left|\det \bigg( \frac{\delta G^a(A^\epsilon)}{\delta\epsilon^b(y)} \bigg)\right|^{-1} \\
\Rightarrow \Delta_{FP} &= \left|\det \bigg( \frac{\delta G^a(A^\epsilon)}{\delta\epsilon^b(y)} \bigg)\right|
\end{align*}
First of we need to calculate the Faddeev-Popov determinant for the axial gauge:
\begin{align*}
A_\mu^\epsilon - A_\mu &= \comm{D_\mu}\epsilon = \del_\mu \epsilon + \i g A_\mu^a \epsilon^b \comm{T^a}{T^b}\\
&= \left( \del_\mu \epsilon^c - g f^{abc} A_\mu^a \epsilon^b \right) T^c \eqqcolon \frac 1 g D_\mu^{cb} \epsilon^b T^c
\end{align*}
This implies that the derivative has the form:
\begin{align*}
\Rightarrow \frac 1 g D_\mu^{ab} = \del_\mu \delta^{ab} + g f^{abc} A_\mu^c \ \left( \equiv \del_\mu \delta^{ab} + \i g (t_A)^c A_\mu^c \right)
\end{align*}
We can use this definition to calculate the Faddeev-Popov determinant:
\begin{align*}
 \frac{\delta G^a(A^\epsilon)}{\delta\epsilon^b(y)} &= n_{\mu} \frac{\delta A^{\mu, \epsilon, a}}{\delta \epsilon^{b}(y)} = n_{\mu} \frac{\delta }{\delta \epsilon^{b}(y)} \bigg(A^{\mu, a} + \frac{1}{g} D^{ab,\mu} \epsilon^{b}(x) \bigg) \\
&= \frac{1}{g} n_{\mu} \big(\partial^{\mu} \delta^{ab} + f^{abc} A^{\mu,c}(x) \big) \delta^{(4)}(x-y) \\
&= \frac{1}{g} n_{\mu} \partial^{\mu} \delta^{ab} \delta^{(4)}(x-y)
\end{align*}
We can see that for our gauge the determinant gets independent of $A$ since $n_{\mu} A^{\mu, a} = 0$. We can therefore just absorb it into the normalization, which we will denote by $\mathcal{N}$. The usual trick is now to insert a $\mathds{1}$ into the Path Integral.
\begin{align*}
&\int \mathcal{D}A \underbrace{\int \mathcal{D}\epsilon(x) \delta(G^a(A^\epsilon))\cdot  \Delta_{FP} }_{\mathds{1}} \cdot  \exp \bigg\{ \i \int d^4 x \bigg(-\frac{1}{4}  {F_{\mu \nu}^{a}}^2 \bigg) \bigg\} \\
=\, &\mathcal{N} \int \mathcal{D}A \int \mathcal{D}\epsilon(x) \delta(G^a(A^\epsilon)) \cdot  \exp \bigg\{ \i \int d^4 x \bigg(-\frac{1}{4}  {F_{\mu \nu}^{a}}^2 \bigg) \bigg\} \\
=\, &\mathcal{N} \int \mathcal{D}A \, \delta(G^a(A)) \cdot  \exp \bigg\{ \i \int d^4 x \bigg(-\frac{1}{4}  {F_{\mu \nu}^{a}}^2 \bigg) \bigg\}
\end{align*}
Where we used gauge independence in the last step and absorbed the group manifold volume into the normalization. The trick now is to replace $\delta(G^a(A))=\delta(n_{\mu} A^{\mu,a}) \to \delta(n_{\mu} A^{\mu,a}+\omega(x))$ and integrate over $\omega$ with the Gaussian weight:
\begin{equation*}
\exp \bigg\{ \i \int d^4 x \bigg(-\frac{1}{2 \xi}  {\omega(x)}^2 \bigg) \bigg\}
\end{equation*}
To return to our initial integral we need to consider $\xi \downarrow 0$, since it will effectively turn the Gaussian into a $\delta$-distribution and therefore brings $\omega$ back to $0$.
\begin{align*}
\lim_{\xi \downarrow 0}\, &\mathcal{N} \int \D A \D \omega \, \delta(n_{\mu} A^{\mu,a}+\omega(x)) \cdot  \exp \bigg\{ \i \int d^4 x \bigg(-\frac{1}{4}  {F_{\mu \nu}^{a}}^2  - \frac{1}{2 \xi} {\omega(x)}^2\bigg) \bigg\} \\
=\lim_{\xi \downarrow 0} \, &\mathcal{N} \int \mathcal{D}A \, \exp \bigg\{ \i \int d^4 x \bigg( \underbrace{-\frac{1}{4}  {F_{\mu \nu}^{a}}^2  - \frac{1}{2 \xi} {(n_{\mu} A^{\mu,a})}^2}_{\mathscr{L}_{\xi}}\bigg) \bigg\}
\end{align*}
Here $\mathscr{L}_{\xi}:=\mathscr{L}_{G}+\mathscr{L}_{GF}$, with the Gauge Fixing term from above. We know the quadratic form of the gauge sector $\mathscr{L}_{G}$ and the form for $\mathscr{L}_{GF}$ can also be easily deduced. 
Keeping in mind that in the end we need to take the limit $\xi \downarrow 0$ we get: 
\begin{equation*}
\mathscr{L}_{\xi} \equiv \frac{1}{2} A_{\mu}^{a}  \underbrace{\bigg(\Box g^{\mu \nu} - \partial^{\mu} \partial^{\nu} - \frac{1}{\xi} n^{\mu} n^{\nu} \bigg) \delta^{ab} }_{\left(\mathscr D^{-1}\right)^{\mu \nu}}  A_{\nu}^{b}  
\end{equation*}
To get the propagator we need to invert the relation within the brackets. In momentum space we get:
\begin{equation*}\label{eq-inv}
\delta^{\mu}_{\nu} \delta^{ab} \hteq \mathscr{D}^{\mu \rho} \bigg(-k^{2} g_{\rho \nu} + k_{\rho} k_{\nu} - \frac{1}{\xi} n_{\rho} n_{\nu} \bigg) \tag{$\ast$}
\end{equation*}
We make the following ansatz:
\begin{equation*}
\mathscr{D}^{\mu \rho} =(A g^{\mu \rho} +B k^{\mu} k^{\rho} + C k^{\mu} n^{\rho} + D n^{\mu} k^{\rho}) \delta^{ab}
\end{equation*}
Inserting this expression int (\ref{eq-inv}), we immediately see that $A=-\frac{1}{k^2}$. Furthermore we get the following system by equation coefficients:
\begin{align*}
0 &\hteq -\frac{1}{k^{2}} + C (n\cdot k)  \\
0 &\hteq \frac{1}{\xi k^{2}} -\frac{1}{\xi} D (n\cdot k) \\
0 &\hteq -C k^{2} -\frac{1}{\xi} C n^{2} -  \frac{1}{\xi} B (n\cdot k)
\end{align*}
From which we deduce the constants:
\begin{align*}
B &= -(\xi k^{2} + n^2 ) \frac{1}{k^{2} (n \cdot k)^{2}} \\
C &= D = \frac{1}{k^{2} (n \cdot k)}
\end{align*}
Inserting everything into our ansatz and restoring the limit, we obtain:
\begin{align*}
\mathscr{D}^{\mu \nu} &= \lim_{\xi \downarrow 0} \dfrac{1}{k^{2}} \bigg( - g^{\mu \nu} - \frac{\xi k^{2} + n^{2}}{(n \cdot k)^{2}} k^{\mu} k^{\nu} + \frac{k^{\mu} n^{\nu} + k^{\nu} n^{\mu}}{n\cdot k} \bigg) \delta^{ab} \\
&= \frac{1}{k^{2}}	\bigg( -g^{\mu \nu} + \frac{k^{\mu} n^{\nu} + k^{\nu} n^{\mu} }{n \cdot k} - n^{2} \frac{k^{\mu} k^{\nu}}{(n \cdot k)^{2}} \bigg) \delta^{ab}
\end{align*}
Adding the causal prescription to shift the denominators away from the axis, we obtain the final result:
\begin{equation*}
\boxed{
\mathscr{D}^{\mu \nu} =  \frac{1}{k^{2} + \i \epsilon}	\bigg( -g^{\mu \nu} + \frac{k^{\mu} n^{\nu} + k^{\nu} n^{\mu} }{n \cdot k + \i \epsilon} - n^{2} \frac{k^{\mu} k^{\nu}}{(n \cdot k)^{2} + \i \epsilon} \bigg) \delta^{ab}
}
\end{equation*}
\end{addmargin}
\noindent\rule{\textwidth}{1pt}
\clearpage
%\fi

% Exercise 2
\noindent\rule{\textwidth}{2pt}
\textbf{Exercise 2a) -- Solution:}\\\
\begin{addmargin}[1cm]{0cm}
\begin{align*}
\feynmandiagram[inline=(d.base), medium, horizontal=d to b] {
	d [particle={\(a, \mu\)}] -- [gluon, momentum'=\(k\)] b [dot], 
	a [particle={\(b, \nu\)}] -- [gluon, momentum'=\(p_1\)] b, 
	c [particle={\(c, \rho\)}] -- [gluon, momentum'=\(p_2\)] b, 
};
 \eqqcolon (M^{\mu\nu\rho})^{abc}
\end{align*}
The contribution for $ggg$ interaction comes from $-\frac14 F_{\mu\nu}^a F^{a,\mu\nu}$ and has to have the form:
\begin{align*}
-\frac 14 (\del_\mu A_\nu^a - \del_\nu A_\mu^a) g f^{abc}A^{b,\mu}A^{c,\nu} = -\frac 14 g f^{abc} (g^{\alpha \nu} g^{\mu \rho} - g^{\alpha \rho} g^{\mu \nu}) (\del_\alpha A_\mu^a) A_\nu^b A_\rho^c
\end{align*}
Summing over all combinations and going to momentum space yields the Feynman-Rule:
\begin{align*}
(M^{\mu\nu\rho})^{abc} = \frac{-i g}{k^2} f^{abc}\left[ g^{\mu\nu}(k - p_1)^\rho + g^{\rho\nu}(p_1 - p_2)^\mu + g^{\rho\mu}(p_2 - k)^\nu \right]
\end{align*}
Contraction with $k_{\mu}$ and the polarization vectors yields:
\begin{align*}
\Rightarrow(k_\mu M^{\mu\nu\rho} \epsilon^*_\nu(p_1) \epsilon^*_\rho(p_2))^{abc} \sim \left\{ 
\begin{matrix}
(p_1 + p_2)^\mu \epsilon^*_\mu(p_1) (2p_1 + p_2)^\nu \epsilon^*_\nu(p_2) \\
- (p_1 + p_2)^\mu (p_1 - p_2)_\mu \epsilon^{*\nu}(p_1)  \epsilon^*_\nu(p_2) \\
- (p_1 + p_2)^\mu \epsilon^*_\mu(p_2) (2p_2 + p_1)^\nu \epsilon^*_\nu(p_1)
\end{matrix} \right. = 0
\end{align*}
In last step we used $p^\mu \epsilon^*_\mu(p) = 0$ and $p_1^2 = p_2^2 = 0$
\QED
\end{addmargin}
\noindent\rule{\textwidth}{1pt}

%\noindent\rule{\textwidth}{2pt}
\textbf{Exercise 2b) -- Solution:}\\\
\begin{addmargin}[1cm]{0cm}
\begin{align*}
\circled 1 :\ 
\feynmandiagram[inline=(b.base), layered layout, medium,horizontal=b to c] {
	a [particle={\(a, \mu\)}] -- [gluon, momentum=\(k\)] b [dot] -- [gluon, half left, momentum=\(k+q\)] 
	c [dot] -- [gluon, half left, momentum=\(q\)] b,
	c -- [gluon,momentum=\(k\)] d [particle={\(b, \nu\)}],
}; ,
&&
\circled 2 :\ 
\feynmandiagram[inline=(b.base), layered layout, medium,horizontal=b to c] {
	a [particle={\(a, \mu\)}] -- [gluon, momentum=\(k\)] b [dot] -- [scalar, half left, momentum=\(k+q\)] 
	c [dot] -- [scalar, half left, momentum=\(q\)] b,
	c -- [gluon,momentum=\(k\)] d [particle={\(b, \nu\)}],
};
\end{align*}
We start by considering \circled 1:
\begin{align*}
\circled 1 \equiv& \i \ ^G\Pi_{\mu\nu}^{ab}(k) = \frac 12 \int \frac{d^dq}{(2\pi)^d} g_s f^{acd} V_{\mu\lambda\rho}(k, q, -k-q) \cdot \i \frac{d^{\lambda \tau}(k+q)}{(k+q)^2} \cdot \i \frac{d^{\rho\sigma}(q)}{(q)^2} \\ &\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \cdot g_s f^{bdc} V_{\nu\sigma\tau}(-k, k+q, -q) \\
=& \frac{g_s^2}2 f^{acd} f^{bcd} \int \frac{d^dq}{(2\pi)^d} \frac 1 {q^2(k+q)^2}\left[ A_{\mu\nu} + (1-\xi) B_{\mu\nu} + (1-\xi)^2 C_{\mu\nu} \right]
\end{align*}
Here $V$ denotes the non-trivial part for 3-point vertices (with all momenta pointing towards the vertex) and $A$, $B$ and $C$ are defined as followed:
\begin{align*}
A_{\mu\nu} \coloneqq & (2q^2 +2kq +5k^2)g_{\mu\nu} + (4d-6)q_\mu q_\nu + (2d-3)(q_\mu k_\nu + q_\nu k_\mu) + (d-6)k_\mu k_\nu\\
B_{\mu\nu} \coloneqq & -\frac{(q^2 + 2kq)^2}{q^2}g_{\mu\nu} + \frac{q^2 + 2kq - k^2}{q^2}q_\mu q_\nu + \frac{q^2 + 3kq}{q^2}(q_\mu k_\nu + q_\nu k_\mu) - k_\mu k_\nu \\& + (q \to q +k, k \to -k) \\
C_{\mu\nu} \coloneqq & \frac{(k^2 q_\mu -kq k_\mu) (k^2 q_\nu -kq k_\nu)}{q^2(q+k)^2}
\end{align*}
Now we set $d=4 - 2\epsilon$ and make use of $f^{acd} f^{bcd} = -f^{acd} f^{dcb} = -\i (t_A^c)_{ad} \i (t_A^c)_{db} = (t_A^c t_A^c)_{ab} = C_A \delta^{ab} \overset{\text{SU(N)}}= N \delta^{ab}$ and $\int d^dk k^{-2}$ \textit{(scaleless integrals vanish in dim.-reg.)}. Plugging our results in and doing a Wick-rotation we end up with:
\begin{align*}
\circled 1 \equiv& \frac{g_s^2}{2(4\pi)^{2-\epsilon}} \delta^{ab} C_A (-k^2)^{-\epsilon} \frac{\Gamma(\epsilon) B(2-\epsilon, 2-\epsilon)}{1-\epsilon} \left[ (19-12\epsilon)k^2g_{\mu\nu} \right. \\& \left.- 2(11-7\epsilon)k_\mu k_\nu + (k^2g_{\mu\nu} - k_\mu k_\nu)(3-2\epsilon)(1-\xi)(2(1-4\epsilon) + (1-\xi)\epsilon) \right]\\
&\text{with } B(x, y) = \frac{\Gamma(x)\Gamma(y)}{\Gamma(x+y)}
\end{align*}
Next, we consider $\circled 2$. With similar arguments we get:
\begin{align*}
\circled 2 \equiv& \i \ ^{FP}\Pi_{\mu\nu}^{ab}(k) = \frac{g_s^2}{2(4\pi)^{2-\epsilon}} \delta^{ab} C_A (-k^2)^{-\epsilon} \frac{\Gamma(\epsilon) B(2-\epsilon, 2-\epsilon)}{1-\epsilon} \left[ k^2 g_{\mu\nu} + 2(1-\epsilon) k_\mu k_\nu \right]
\end{align*}
We now make the definition $X_{\mu\nu} \coloneqq \circled 1 + \circled 2 = \i \ ^G\Pi_{\mu\nu}^{ab}(k) + \i \ ^{FP}\Pi_{\mu\nu}^{ab}(k)$ and note:
\begin{align*}
X_{\mu\nu} =& \frac{g_s^2}{2(4\pi)^{2-\epsilon}} \delta^{ab} C_A (-k^2)^{-\epsilon} (k^2 g_{\mu\nu} - k_\mu k_\nu) \frac{\Gamma(\epsilon) B(2-\epsilon, 2-\epsilon)}{1-\epsilon} \\ &\left[ 2(5-3\epsilon) + (1-\xi)(1-4\epsilon)(3-2\epsilon) + (1-\xi)^2 \frac \epsilon 2(3-2\epsilon) \right]\\
\sim& k^2 g_{\mu\nu} - k_\mu k_\nu\\
\Rightarrow k^\mu X_{\mu\nu} =& 0
\end{align*}
\QED
\end{addmargin}
\noindent\rule{\textwidth}{1pt}
\clearpage

%\iffalse
% Exercise 3
\noindent\rule{\textwidth}{2pt}
\textbf{Exercise 3 -- Solution:}\\\
\begin{addmargin}[1cm]{0cm}
We want to check 
\begin{equation*}
\delta^{2} A^{a}_{\mu} = 0
\end{equation*}
We need the following relations:
\begin{equation*}\label{eq-delta}
\delta_{\lambda} A^{a}_{\mu} = \lambda (D_{\mu} c)^{a}, \qquad \delta_{\lambda} c^{a} = - \dfrac{1}{2} g \lambda f^{abc} c^{b} c^{c}, \qquad \delta_\lambda = \lambda \delta \tag{$\ast$}
\end{equation*}
Using these relations yields:
\begin{align*}
\delta( \delta A^{a}_{\mu} ) &= (\delta D_{\mu}^{ab} ) c^{b} + D_{\mu}^{ab} (\delta c^{b}) \\
&= (\delta (\partial_{\mu} \delta^{ab} + g f^{acb} A_{\mu}^{c}) ) c^{b} + D_{\mu}^{ab} (\delta c^{b}) \\
&= g f^{acb} (\delta A_{\mu}^{c}) c^{b} + \partial_{\mu} \delta^{ab} (\delta c^{b}) + g f^{acb} A_{\mu}^{c} (\delta c^{b}) = \ldots
\end{align*}
Now we can insert (\ref{eq-delta}) into our equation above. We obtain:
\begin{align*}
\ldots &= g f^{acb} (D_{\mu}^{cd} c^{d}) c^{b} + \partial_{\mu} \bigg( -\frac{1}{2} g f^{abc} c^{b} c^{c} \bigg) + g f^{acb} A_{\mu}^{c} \bigg( -\frac{1}{2} g f^{bde} c^{d} c^{e} \bigg) \\
&= g f^{acb} (\partial_{\mu} \delta^{cd} c^{d}) c^{b} + g^{2} f^{acb} f^{ced} A_{\mu}^{e} c^{d} c^{b} - \frac{1}{2} g f^{abc} \partial_{\mu} (c^{b} c^{c}) - \frac{1}{2} g^{2} f^{acb} f^{bde} A_{\mu}^{c} c^{d} c^{e} \\
&= g f^{acb} (\partial_{\mu} c^{c}) c^{b} -\frac{1}{2} g f^{abc} \partial_{\mu} (c^{b} c^{c}) +g^{2} f^{abc} f^{ced} A_{\mu}^{e} c^{d} c^{e} - \frac{1}{2} g^{2} f^{acb} f^{bde} A_{\mu}^{c} c^{d} c^{e} \\
&=  g f^{acb} (\partial_{\mu} c^{c}) c^{b} -\frac{1}{2} g f^{abc}( (\partial_{\mu} c^{b})c^{c} + c^{b} (\partial_{\mu} c^{c})) +g^{2} f^{abc} f^{ced} A_{\mu}^{e} c^{d} c^{e} - \frac{1}{2} g^{2} f^{acb} f^{bde} A_{\mu}^{c} c^{d} c^{e} = \ldots
\end{align*}
Now we can rename indices in the second term and use the anticommuting property of the Grassmann fields $c$ to cancel the first term. We remain with:
\begin{align*}
\ldots &= g^{2} f^{abc} f^{ced} A_{\mu}^{e} c^{d} c^{b} - \frac{1}{2} g^{2} f^{acb} f^{bde} A_{\mu}^{c} c^{d} c^{e} \\
&= \frac{1}{2} g^{2} A^{e}_{\mu} c^{d} c^{b} (2 f^{abc} f^{ced} - f^{aec} f^{cdb} ) \\
&= - \frac{1}{2} g^{2} A^{e}_{\mu} c^{d} c^{b} (\underbrace{f^{bca} f^{dec} + f^{acb} f^{edc} + f^{eca} f^{bdc}}_{=0\: \text{Jacobi identitiy}}) = 0 
\end{align*}
\QED
\end{addmargin}
\noindent\rule{\textwidth}{1pt}
%\fi
\end{document}

